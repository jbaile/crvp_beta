
#include "FGLP.h"

using namespace llvm;

// Note: Using basic block to indicate the inner most loop that the block lives in
bool CRVPProfileFileDump(std::vector<Function*>& ProfFuncVec, 
												std::vector<BasicBlock*>& ProfLoopVec,
												std::vector<Instruction*>& ProfInstVec,
												CRVPInit* CI){
	std::ofstream of;
	of.open("Profile.txt", std::ofstream::out);
	for(Function* F : ProfFuncVec)
		of << FUNC_TYPE_DEF << " " << CI->CPI.FuncToIdMap[F] << "\n";
	for(BasicBlock* B : ProfLoopVec)
		of << LOOP_TYPE_DEF << " " << CI->CPI.BBToIdMap[B] << "\n";
	for(Instruction* I : ProfInstVec)
		of << INST_TYPE_DEF << " " << CI->CPI.ValToIdMap[I] << "\n";
	of.close();
}
