/* 
 * stats.c
 * Author:Sunghyun Park, CCCP Group, Umich
 *
 */

#include "stats.h"
#define MARGIN_ERROR 0.0001

static inline
__attribute__((always_inline))
int eq(double a, double b)
{
  return (abs(a-b) < MARGIN_ERROR)? 1:0;
}

void insert_to_table_int(int num_steady, int num_clear, /*int num_input, */int* pos_lfe_steady, int* pos_lfe_clear, 
	int clear, int threshold, uint64_t* TNV, int value, int num_profiled, int* tic)
{
  int num_total = num_steady+num_clear;
  int pos_clear = num_steady;
  // CHECK : hash table?
  int i,j,k;
  int clear_interval = threshold;
  if(TNV[*pos_lfe_steady*2+1] > threshold) clear_interval = 2*TNV[*pos_lfe_steady*2+1];
  
  assert(*pos_lfe_steady >= 0 && *pos_lfe_steady < num_steady);
  assert(*pos_lfe_clear >= pos_clear && *pos_lfe_clear < num_total);
  (*tic)++;
  
  //flush clear entries
  if((clear!=0) && (*tic > clear_interval)){
    for(j=pos_clear;j<num_total;++j){
      TNV[2*j] = 0;
      TNV[2*j+1] = 0;
    }
    *tic = 0;
    *pos_lfe_clear = num_steady; 
  }

  int found = 0;
  int found_idx = 0;
  for(j=0;j<num_total;++j)
  {
    if( (TNV[j*2] == value) && !( (value==0) && (TNV[j*2+1] == 0)) ) {
      found = 1;
      found_idx = j;
      break;
    }
  }

  if(found){
    TNV[found_idx*2+1] += num_profiled;
    //if found at clear entries, compare with lfe at steady entries
    if((found_idx >= pos_clear) && (TNV[*pos_lfe_steady*2+1] < TNV[found_idx*2+1])){
        TNV[*pos_lfe_steady*2] = TNV[found_idx*2];
        TNV[*pos_lfe_steady*2+1] = TNV[found_idx*2+1]; 
        TNV[found_idx*2] = 0;
        TNV[found_idx*2+1] = 0;
    }
  }
  else {
    // steady part is not yet filled
    if(TNV[*pos_lfe_steady*2+1] < 1){
      TNV[*pos_lfe_steady*2] = value;
      TNV[*pos_lfe_steady*2+1] = num_profiled;
    }
    else if(TNV[*pos_lfe_steady*2+1] < num_profiled){
      TNV[*pos_lfe_clear*2] = TNV[*pos_lfe_steady*2];
      TNV[*pos_lfe_clear*2+1] = TNV[*pos_lfe_steady*2+1];

      TNV[*pos_lfe_steady*2] = value;
      TNV[*pos_lfe_steady*2+1] = num_profiled;
    } 
    else if(TNV[*pos_lfe_clear*2+1] < num_profiled)
    {
      // evict lfe at clear entries
      TNV[*pos_lfe_clear*2] = value;
      TNV[*pos_lfe_clear*2+1] = num_profiled;
    }
  }

  int min_freq = INT_MAX;
  int min_idx;
  for(k=0;k<num_steady;++k){
    if(min_freq > TNV[k*2+1]){
      min_freq = TNV[k*2+1];
      min_idx = k;
    }
  }
  *pos_lfe_steady = min_idx;

  min_freq = INT_MAX;
  for(k=pos_clear;k<num_total;++k){
    if(min_freq > TNV[k*2+1]){
      min_freq = TNV[k*2+1];
      min_idx = k;
    }
  }
  *pos_lfe_clear = min_idx;
}

void insert_to_table_double(int num_steady, int num_clear, /*int num_input, */int* pos_lfe_steady, int* pos_lfe_clear, 
	int clear, int threshold, double* TNV, double value, int num_profiled, int* tic)
{
  int num_total = num_steady+num_clear;
  int pos_clear = num_steady;
  // CHECK : hash table?
  int i,j,k;
  int clear_interval = threshold;
  if(TNV[*pos_lfe_steady*2+1] > threshold) clear_interval = 2*TNV[*pos_lfe_steady*2+1];
  
  assert(*pos_lfe_steady >= 0 && *pos_lfe_steady < num_steady);
  assert(*pos_lfe_clear >= pos_clear && *pos_lfe_clear < num_total);
  (*tic)++;
  
  //flush clear entries
  if((clear!=0) && (*tic > clear_interval)){
    for(j=pos_clear;j<num_total;++j){
      TNV[2*j] = 0;
      TNV[2*j+1] = 0;
    }
    *tic = 0;
    *pos_lfe_clear = num_steady; 
  }

  int found = 0;
  int found_idx = 0;
  for(j=0;j<num_total;++j)
  {
    if( eq(TNV[j*2],value) && !(eq(value, 0.0) && (TNV[j*2+1] == 0)) ) {
      found = 1;
      found_idx = j;
      break;
    }
  }

  if(found){
    TNV[found_idx*2+1] += num_profiled;
    //if found at clear entries, compare with lfe at steady entries
    if((found_idx >= pos_clear) && (TNV[*pos_lfe_steady*2+1] < TNV[found_idx*2+1])){
        TNV[*pos_lfe_steady*2] = TNV[found_idx*2];
        TNV[*pos_lfe_steady*2+1] = TNV[found_idx*2+1]; 
        TNV[found_idx*2] = 0;
        TNV[found_idx*2+1] = 0;
    }
  }
  else {
    // steady part is not yet filled
    if(TNV[*pos_lfe_steady*2+1] < 1){
      TNV[*pos_lfe_steady*2] = value;
      TNV[*pos_lfe_steady*2+1] = num_profiled;
    }
    else if(TNV[*pos_lfe_steady*2+1] < num_profiled){
      TNV[*pos_lfe_clear*2] = TNV[*pos_lfe_steady*2];
      TNV[*pos_lfe_clear*2+1] = TNV[*pos_lfe_steady*2+1];

      TNV[*pos_lfe_steady*2] = value;
      TNV[*pos_lfe_steady*2+1] = num_profiled;
    } 
    else if(TNV[*pos_lfe_clear*2+1] < num_profiled)
    {
      // evict lfe at clear entries
      TNV[*pos_lfe_clear*2] = value;
      TNV[*pos_lfe_clear*2+1] = num_profiled;
    }
  }

  int min_freq = INT_MAX;
  int min_idx;
  for(k=0;k<num_steady;++k){
    if(min_freq > TNV[k*2+1]){
      min_freq = TNV[k*2+1];
      min_idx = k;
    }
  }
  *pos_lfe_steady = min_idx;

  min_freq = INT_MAX;
  for(k=pos_clear;k<num_total;++k){
    if(min_freq > TNV[k*2+1]){
      min_freq = TNV[k*2+1];
      min_idx = k;
    }
  }
  *pos_lfe_clear = min_idx;
}

/*
void getTNV_int(int num_steady, int num_clear, int num_input, int clear, int threshold, int* TNV, int* input)
{
  int tic=0;
  int last_value = input[0];
  int num_profiled = 1;
  int i;
  int pos_lfe_steady = 0;
  int pos_lfe_clear = num_steady;

  for(i=1;i<num_input;++i){
    if(last_value == input[i]){
      num_profiled++;
    }
    else{
      insert_to_table_int(num_steady, num_clear, num_input, &pos_lfe_steady, &pos_lfe_clear, clear, threshold, TNV, last_value, num_profiled, &tic);
      last_value = input[i];
      num_profiled = 1;
    }
  }
  insert_to_table_int(num_steady, num_clear, num_input, &pos_lfe_steady, &pos_lfe_clear, clear, threshold, TNV, last_value, num_profiled, &tic);
  
  printf("\n\n");
  for(i=0;i<num_steady;++i) {
    printf("%d : %d\n", TNV[2*i], TNV[2*i+1]);
  }
}

void getTNV_double(int num_steady, int num_clear, int num_input, int clear, int threshold, double* TNV, double* input)
{
  int tic=0;
  double last_value = input[0];
  int num_profiled = 1;
  int i;
  int pos_lfe_steady = 0;
  int pos_lfe_clear = num_steady;

  for(i=1;i<num_input;++i){
    if(last_value == input[i]){
      num_profiled++;
    }
    else{
      insert_to_table_double(num_steady, num_clear, num_input, &pos_lfe_steady, &pos_lfe_clear, clear, threshold, TNV, last_value, num_profiled, &tic);
      last_value = input[i];
      num_profiled = 1;
    }
  }
  insert_to_table_double(num_steady, num_clear, num_input, &pos_lfe_steady, &pos_lfe_clear, clear, threshold, TNV, last_value, num_profiled, &tic);
  
  printf("\n\n");
  for(i=0;i<num_steady;++i) {
    printf("%d : %d\n", TNV[2*i], TNV[2*i+1]);
  }
}

void getGeneralStats_int(int num_input, int* inputs, double* outputs){
  int min = INT_MAX, max = INT_MIN, sum=0;
  double avg, davg, var, dsum = 0.0, zero_ratio;
  int zero_cnt = 0;
  int i;

  for(i=0;i<num_input;++i){
    min = (min>inputs[i])? inputs[i] : min;
    max = (max<inputs[i])? inputs[i] : max;
    zero_cnt = (inputs[i] == 0)? zero_cnt+1 : zero_cnt;
    sum += inputs[i];
    dsum += inputs[i] * inputs[i];
  }
  avg = sum / (double)num_input;
  davg = dsum / (double)num_input;

  for(i=0;i<num_input;++i){
    dsum += (double)(inputs[i]-avg)*(inputs[i]-avg);
  }
  var = dsum / (double)num_input;
  zero_ratio = (double)zero_cnt / (double)num_input * 100.0;

  outputs[0] = min;
  outputs[1] = max;
  outputs[2] = avg;
  outputs[3] = davg;
  outputs[4] = zero_ratio;
  //printf("Min : %d, Max : %d, Avg : %f, Var : %f, Zero : %f%c\n", min, max, avg, var, zero_ratio, '%');

}

void getGeneralStats_double(int num_input, double* inputs, double* outputs){
  double min = DBL_MAX, max = DBL_MIN, sum=0.0;
  double avg, davg, var, dsum = 0.0, zero_ratio;
  int zero_cnt = 0;
  int i;

  for(i=0;i<num_input;++i){
    min = (min>inputs[i])? inputs[i] : min;
    max = (max<inputs[i])? inputs[i] : max;
    zero_cnt = (inputs[i] == 0)? zero_cnt+1 : zero_cnt;
    sum += inputs[i];
    dsum += inputs[i] * inputs[i];
  }
  avg = sum / (double)num_input;
  davg = dsum / (double)num_input;

  for(i=0;i<num_input;++i){
    dsum += (double)(inputs[i]-avg)*(inputs[i]-avg);
  }
  var = dsum / (double)num_input;
  zero_ratio = (double)zero_cnt / (double)num_input * 100.0;

  outputs[0] = min;
  outputs[1] = max;
  outputs[2] = avg;
  outputs[3] = davg;
  outputs[4] = zero_ratio;
  //printf("Min : %d, Max : %d, Avg : %f, Var : %f, Zero : %f%c\n", min, max, avg, var, zero_ratio, '%');

}
*/
