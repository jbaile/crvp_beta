/* 
 * CRVP_RT.h
 * Author: Shikai Li, CCCP Group, Umich
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <assert.h>

#include "../CRVP_Types.h"
#include "stats.h"

typedef _Bool uint1_t;

struct STATS_BOOL* BOOL_DATA_PTR;
uint32_t BOOL_DATA_SIZE = 0;

struct STATS_INT* INT_DATA_PTR[4];
uint32_t INT_DATA_SIZE[4] = {0};

struct STATS_FP* FP_DATA_PTR[2];
uint32_t FP_DATA_SIZE[2] = {0};

void STATS_BOOL_IMPL( struct STATS_BOOL* PTR, uint32_t VALID, uint1_t VAL );
void STATS_INT_IMPL( struct STATS_INT* PTR, uint32_t VALID, uint64_t UVAL, int64_t VAL );
void STATS_FP_IMPL( struct STATS_FP* PTR, uint32_t VALID, double VAL );

void CRVP_MODULE_INIT( );

void CRVP_LOOP_BACK( uint32_t LOOP_ID );
void CRVP_LOOP_EXIT( uint32_t LOOP_ID );

void CRVP_VAL_INT_1( uint32_t VAL_ID, uint1_t VAL );
void CRVP_VAL_INT_8( uint32_t VAL_ID, uint8_t VAL );
void CRVP_VAL_INT_16( uint32_t VAL_ID, uint16_t VAL );
void CRVP_VAL_INT_32( uint32_t VAL_ID, uint32_t VAL );
void CRVP_VAL_INT_64( uint32_t VAL_ID, uint64_t VAL );
void CRVP_VAL_FP_32( uint32_t VAL_ID, float VAL );
void CRVP_VAL_FP_64( uint32_t VAL_ID, double VAL );
