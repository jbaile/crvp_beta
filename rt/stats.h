/* 
 * stats.h
 * Author:Sunghyun Park, CCCP Group, Umich
 *
 */

#include <limits.h>
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <float.h>

// Get Top N values
// Parameter Settings 
// num_steady = How many Top N values do you want?
// num_clear = 2*num_steady
// num_input = size of input dataset
// clear : 1 - flush regularly(Default), 0 - don't flush at all.  
// threshold : minimum clear interval. As minimum value at steady entries has larger value, then increase the interval as twice as minimum value at steady entries.
// TNV : Output. return Top N values
// input : Input
//void getTNV_int(int num_steady, int num_clear, int num_input, int clear, int threshold, int* TNV, int* input);
//void getTNV_double(int num_steady, int num_clear, int num_input, int clear, int threshold, double* TNV, double* input);

void insert_to_table_int(int num_steady, int num_clear, /*int num_input, */int* pos_lfe_steady, int* pos_lfe_clear, 
	int clear, int threshold, uint64_t* TNV, int value, int num_profiled, int* tic);
void insert_to_table_double(int num_steady, int num_clear, /*int num_input, */int* pos_lfe_steady, int* pos_lfe_clear, 
	int clear, int threshold, double* TNV, double value, int num_profiled, int* tic);

// Get General Stats
// outputs[0] = min
// outputs[1] = max
// outputs[2] = E[X]
// outputs[3] = E[X^2]
// outputs[4] = percent of zero values
//void getGeneralStats_int(int num_input, int* inputs, double* outputs);
//void getGeneralStats_double(int num_input, double* inputs, double* outputs);

