/* 
 * CRVP_RT.c
 * Author: Shikai Li, CCCP Group, Umich
 *
 */
#include "CRVP_RT.h"

void CRVP_MODULE_INIT( ){
  FILE *fp;
  fp = fopen("anlysdata.crvp", "rb");
  
  if(fp == NULL){
    printf("Couldn't open the analysis data file. Profiling determinates.\n");
    exit(0);
  }

  struct ANLYS_SUM AS;
  fread(&AS, sizeof(struct ANLYS_SUM), 1, fp);

  int i = 0, j = 0;  
  BOOL_DATA_SIZE     = AS.PROF_SIZE[0];
  BOOL_DATA_PTR     = (struct STATS_BOOL*) malloc ( BOOL_DATA_SIZE * sizeof(struct STATS_BOOL) );
  for( i = 0; i < BOOL_DATA_SIZE; i ++ )
    BOOL_DATA_PTR[i] = (struct STATS_BOOL){ .CNT_0 = 0, .CNT_1 = 0 };
  
  for( i = 0; i < 4; i ++ ){
    INT_DATA_SIZE[i] = AS.PROF_SIZE[i+1];
    INT_DATA_PTR[i] = (struct STATS_INT*) malloc ( INT_DATA_SIZE[i] * sizeof(struct STATS_INT) );
    for( j = 0; j < INT_DATA_SIZE[i]; j ++ ){
      INT_DATA_PTR[i][j] = (struct STATS_INT){ .CNT = 0, .MAX = LLONG_MIN, .MIN = LLONG_MAX, .SUM = 0, .SUM_2 = 0,
                            .CNT_0 = 0, .UMAX = 0, .UMIN = ULLONG_MAX, .USUM = 0, .USUM_2 = 0, .TNV = {0} };  
      INT_DATA_PTR[i][j].TNV_PTR = (struct STATS_INT_TNV*) malloc ( sizeof(struct STATS_INT_TNV) );
      *INT_DATA_PTR[i][j].TNV_PTR = (struct STATS_INT_TNV){ .TNV = {0}, .tic = 0, .last_value = 0,
                                    .num_profiled = 0, .pos_lfe_steady = 0, .pos_lfe_clear = NUM_STEADY}; 
    }
  }

  for( i = 0; i < 2; i ++ ){
    FP_DATA_SIZE[i] = AS.PROF_SIZE[i+5];
    FP_DATA_PTR[i] = (struct STATS_FP*) malloc ( FP_DATA_SIZE[i] * sizeof(struct STATS_FP) );
    for( j = 0; j < FP_DATA_SIZE[i]; j ++ ){
      FP_DATA_PTR[i][j] = (struct STATS_FP){ .CNT = 0, .MAX = DBL_MIN, .MIN = DBL_MAX, .SUM = 0, .SUM_2 = 0,
                            /*._SUM_A = 0, ._SUM_A_2 = 0, ._SUM_B = 0, ._SUM_B_2 = 0, */.TNV = {0} };  
      FP_DATA_PTR[i][j].TNV_PTR = (struct STATS_FP_TNV*) malloc ( sizeof(struct STATS_FP_TNV) );
      *FP_DATA_PTR[i][j].TNV_PTR = (struct STATS_FP_TNV){ .TNV = {0}, .tic = 0, .last_value = 0,
                                    .num_profiled = 0, .pos_lfe_steady = 0, .pos_lfe_clear = NUM_STEADY }; 
    }
  }
  
  fclose(fp);
};

void CRVP_MODULE_EXIT( ){
  FILE *fp;
  fp = fopen("profdata.crvp", "wb");
  
  if(fp == NULL){
    printf("Couldn't create the profile data file. Profiling determinates.\n");
    exit(0);
  }
  
  // Finish Top N Value Statistic
  int i = 0, j = 0, k = 0;
  for( i = 0; i < 4; i ++ ){
    for( j = 0; j < INT_DATA_SIZE[i]; j ++ ){
      struct STATS_INT_TNV* TP = INT_DATA_PTR[i][j].TNV_PTR;
      
      insert_to_table_int(NUM_STEADY, NUM_CLEAR, /*num_input,*/ 
        &(TP->pos_lfe_steady), &(TP->pos_lfe_clear), 1, 10, 
        &(TP->TNV[0]), TP->last_value, TP->num_profiled, &(TP->tic));
      
      for( k = 0; k < NUM_STEADY*2; k ++ )
        INT_DATA_PTR[i][j].TNV[k] = TP->TNV[k];
    }
  }
  
  for( i = 0; i < 2; i ++ ){
    for( j = 0; j < FP_DATA_SIZE[i]; j ++ ){
      /*
      struct STATS_FP* SFP = &FP_DATA_PTR[i][j];
      SFP->SUM += SFP->_SUM_A + SFP->_SUM_B;
      SFP->SUM_2 += SFP->_SUM_A_2 + SFP->_SUM_B_2;
      */
      
      struct STATS_FP_TNV* TP = FP_DATA_PTR[i][j].TNV_PTR;
      insert_to_table_double(NUM_STEADY, NUM_CLEAR, /*num_input,*/ 
        &(TP->pos_lfe_steady), &(TP->pos_lfe_clear), 1, 10, 
        &(TP->TNV[0]), TP->last_value, TP->num_profiled, &(TP->tic));
      
      for( k = 0; k < NUM_STEADY*2; k ++ )
        FP_DATA_PTR[i][j].TNV[k] = TP->TNV[k];
    }
  }
  
  // Write Profiling Result to Binary File
  fwrite(BOOL_DATA_PTR, sizeof(struct STATS_BOOL), BOOL_DATA_SIZE, fp);
  
  for( i = 0; i < 4; i ++ )
    fwrite(INT_DATA_PTR[i], sizeof(struct STATS_INT), INT_DATA_SIZE[i], fp);
  
  for( i = 0; i < 2; i ++ )
    fwrite(FP_DATA_PTR[i], sizeof(struct STATS_FP), FP_DATA_SIZE[i], fp);
  
  fclose(fp);
};

void CRVP_LOOP_BACK( uint32_t LOOP_ID ){
  // DOING NOTHING
};

void CRVP_LOOP_EXIT( uint32_t LOOP_ID ){
  // DOING NOTHING
};

void STATS_BOOL_IMPL( struct STATS_BOOL* DATA_PTR, uint32_t VALID, uint1_t VAL ){  
  if(VAL)
    DATA_PTR[VALID].CNT_1++;
  else
    DATA_PTR[VALID].CNT_0++;
};

void STATS_INT_IMPL( struct STATS_INT* DATA_PTR, uint32_t VALID, uint64_t UVAL, int64_t VAL ){  
  struct STATS_INT* DP = &DATA_PTR[VALID];
  DP->CNT ++;

  DP->MAX = DP->MAX > VAL ? DP->MAX : VAL;
  DP->MIN = DP->MIN < VAL ? DP->MIN : VAL;
  DP->SUM += VAL;
  DP->SUM_2 += VAL * VAL;
  
  DP->UMAX = DP->UMAX > UVAL ? DP->UMAX : UVAL;
  DP->UMIN = DP->UMIN < UVAL ? DP->UMIN : UVAL;
  DP->USUM += UVAL;
  DP->USUM_2 += UVAL * UVAL;
  
  if(UVAL == 0)
    DP->CNT_0 ++;

  // TOP N Value Statistic  
  if(DP->CNT == 1){
    DP->TNV_PTR->last_value = UVAL;
    DP->TNV_PTR->num_profiled ++;
    return;
  }
  
   if(DP->TNV_PTR->last_value == UVAL){
    DP->TNV_PTR->num_profiled ++;
  }
  else{
    insert_to_table_int(NUM_STEADY, NUM_CLEAR, /*num_input,*/ 
      &(DP->TNV_PTR->pos_lfe_steady), &(DP->TNV_PTR->pos_lfe_clear), 1, 10, 
      &(DP->TNV_PTR->TNV[0]), DP->TNV_PTR->last_value, DP->TNV_PTR->num_profiled, &(DP->TNV_PTR->tic));
    
    DP->TNV_PTR->last_value = UVAL;
    DP->TNV_PTR->num_profiled = 1;
  }
}

void STATS_FP_IMPL( struct STATS_FP* DATA_PTR, uint32_t VALID, double VAL ){
  struct STATS_FP* DP = &DATA_PTR[VALID];
  DP->CNT ++;
  
  DP->MAX = DP->MAX > VAL ? DP->MAX : VAL;
  DP->MIN = DP->MIN < VAL ? DP->MIN : VAL;
  DP->SUM += VAL;
  DP->SUM_2 += VAL * VAL;

  /*
  DP->_SUM_B += VAL;
  DP->_SUM_B_2 += VAL * VAL;
  if( DP->CNT<<24 == 0 ){
    DP->_SUM_A += DP->_SUM_B;
    DP->_SUM_A_2 += DP->_SUM_B_2;
    DP->_SUM_B = 0;
    DP->_SUM_B_2 = 0;
    if( DP->CNT<<16 == 0 ){
      DP->SUM += DP->_SUM_A;
      DP->SUM_2 += DP->_SUM_A_2;
      DP->_SUM_A = 0;
      DP->_SUM_A_2 = 0;
    }
  }
  */

  // TOP N Value Statistic  
  if(DP->CNT == 1){
    DP->TNV_PTR->last_value = VAL;
    DP->TNV_PTR->num_profiled ++;
    return;
  }
  
   if(DP->TNV_PTR->last_value == VAL){
    DP->TNV_PTR->num_profiled ++;
  }
  else{
    insert_to_table_double(NUM_STEADY, NUM_CLEAR, /*num_input,*/ 
      &(DP->TNV_PTR->pos_lfe_steady), &(DP->TNV_PTR->pos_lfe_clear), 1, 10, 
      &(DP->TNV_PTR->TNV[0]), DP->TNV_PTR->last_value, DP->TNV_PTR->num_profiled, &(DP->TNV_PTR->tic));
    
    DP->TNV_PTR->last_value = VAL;
    DP->TNV_PTR->num_profiled = 1;
  }
}

//__attribute__((always_inline))
void CRVP_VAL_INT_1( uint32_t VAL_ID, uint1_t VAL ){
  assert( VAL_ID < BOOL_DATA_SIZE );
  STATS_BOOL_IMPL( BOOL_DATA_PTR, VAL_ID, VAL);
};

void CRVP_VAL_INT_8( uint32_t VAL_ID, uint8_t VAL ){
  assert( VAL_ID < INT_DATA_SIZE[0] );
  STATS_INT_IMPL( INT_DATA_PTR[0], VAL_ID, VAL, *(int8_t*)(&VAL) );
};

void CRVP_VAL_INT_16( uint32_t VAL_ID, uint16_t VAL ){
  assert( VAL_ID < INT_DATA_SIZE[1] );
  STATS_INT_IMPL( INT_DATA_PTR[1], VAL_ID, VAL, *(int16_t*)(&VAL) );
};

void CRVP_VAL_INT_32( uint32_t VAL_ID, uint32_t VAL ){
  assert( VAL_ID < INT_DATA_SIZE[2] );
  STATS_INT_IMPL( INT_DATA_PTR[2], VAL_ID, VAL, *(int32_t*)(&VAL) );
};

void CRVP_VAL_INT_64( uint32_t VAL_ID, uint64_t VAL ){
  assert( VAL_ID < INT_DATA_SIZE[3] );
  STATS_INT_IMPL( INT_DATA_PTR[3], VAL_ID, VAL, *(int64_t*)(&VAL) );
};

void CRVP_VAL_FP_32( uint32_t VAL_ID, float VAL ){
  assert( VAL_ID < FP_DATA_SIZE[0] );
  STATS_FP_IMPL( FP_DATA_PTR[0], VAL_ID, (double) VAL );
};

void CRVP_VAL_FP_64( uint32_t VAL_ID, double VAL ){
  assert( VAL_ID < FP_DATA_SIZE[1] );
  STATS_FP_IMPL( FP_DATA_PTR[1], VAL_ID, VAL);
};
