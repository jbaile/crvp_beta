/* 
 * CRVP_Load.cpp
 * Author: Shikai Li
 *
 */

#include "CRVP_Load.h"

bool CRVPLoad::LoadCRVPProfData(std::string AnlysFileName, std::string ProfFileName){
	if( CLI.valid )
		return false;

	FILE* af;
	af = fopen( AnlysFileName.c_str(), "rb");
	assert( af && "Couldn't find analysis data file.");
	
	ANLYS_SUM& AS = CLI.AS;
	fread( &AS, sizeof(ANLYS_SUM), 1, af );
	
	ANLYS_INFO* AI_PTR = (ANLYS_INFO*) malloc ( AS.ANLYS_SIZE * sizeof(ANLYS_INFO) );
	fread( AI_PTR, sizeof(ANLYS_INFO), AS.ANLYS_SIZE, af );	
	
	std::vector<ANLYS_INFO>& AI_VEC = CLI.AI_VEC;
	AI_VEC.reserve(AS.ANLYS_SIZE);
	AI_VEC.assign(AI_PTR, AI_PTR+AS.ANLYS_SIZE);
	free(AI_PTR);
	
	fclose(af);

	FILE* pf;
	pf = fopen( ProfFileName.c_str(), "rb");
	assert( pf && "Couldn't find profile data file.");

	STATS_BOOL* SB_PTR = (STATS_BOOL*) malloc ( AS.PROF_SIZE[0] * sizeof(STATS_BOOL) );
	fread( SB_PTR, sizeof(STATS_BOOL), AS.PROF_SIZE[0], pf );
	
	std::vector<STATS_BOOL>& SB_VEC = CLI.SB_VEC;
	SB_VEC.reserve(AS.PROF_SIZE[0]);
	SB_VEC.assign(SB_PTR, SB_PTR+AS.PROF_SIZE[0]);
	free(SB_PTR);

	std::vector< std::vector<STATS_INT> >& SI_VEC = CLI.SI_VEC;
	for(int i = 0; i< 4; i++){
		STATS_INT* SI_PTR = (STATS_INT*) malloc ( AS.PROF_SIZE[i+1] * sizeof(STATS_INT) );
		fread( SI_PTR, sizeof(STATS_INT), AS.PROF_SIZE[i+1], pf );
		
		std::vector<STATS_INT> SI_VEC_UNIT;
		SI_VEC_UNIT.reserve(AS.PROF_SIZE[i+1]);
		SI_VEC_UNIT.assign(SI_PTR, SI_PTR+AS.PROF_SIZE[i+1]);
		free(SI_PTR);

		SI_VEC.push_back(SI_VEC_UNIT);
	}
	
	std::vector< std::vector<STATS_FP> >& SF_VEC = CLI.SF_VEC;
	for(int i = 0; i< 2; i++){
		STATS_FP* SF_PTR = (STATS_FP*) malloc ( AS.PROF_SIZE[i+5] * sizeof(STATS_FP) );
		fread( SF_PTR, sizeof(STATS_FP), AS.PROF_SIZE[i+5], pf );
		
		std::vector<STATS_FP> SF_VEC_UNIT;
		SF_VEC_UNIT.reserve(AS.PROF_SIZE[i+5]);
		SF_VEC_UNIT.assign(SF_PTR, SF_PTR+AS.PROF_SIZE[i+5]);
		free(SF_PTR);

		SF_VEC.push_back(SF_VEC_UNIT);
	}

	fclose(pf);

	CLI.valid = true;
	return true;
};

bool CRVPLoad::GetProfData(unsigned int ProfType, unsigned int ProfID, STATS_UNI& SU){
	assert(CLI.valid && "CRVP analysis and profile data are not loaded!");

	SU = {.SB = NULL, .SI = NULL, .SF = NULL};
	if(ProfType == 0){
		SU.SB = &CLI.SB_VEC[ProfID];
		return true;
	}
	if(ProfType >= 1 && ProfType <=4){
		SU.SI = &CLI.SI_VEC[ProfType-1][ProfID];
		return true;
	}
	if(ProfType >= 5 && ProfType <=6){
		SU.SF = &CLI.SF_VEC[ProfType-5][ProfID];
		return true;
	}	
	return false;
};

std::set<Function*> CRVPLoad::GetProfFuncSet(){
	std::set<Function*> ProfFuncSet;
	for(ANLYS_INFO AI: CLI.AI_VEC){
		if(AI.ANLYS_TYPE == FUNC_TYPE_DEF){
			ProfFuncSet.insert(CPI->IdToFuncMap[AI.ANLYS_ID]);
		}
	}
	return ProfFuncSet;
}

std::set<BasicBlock*> CRVPLoad::GetProfLoopBlockSet(){
	std::set<BasicBlock*> ProfLoopBlockSet;
	for(ANLYS_INFO AI: CLI.AI_VEC){
		if(AI.ANLYS_TYPE == LOOP_TYPE_DEF){
			ProfLoopBlockSet.insert(CPI->IdToBBMap[AI.ANLYS_ID]);
		}
	}
	return ProfLoopBlockSet;
}

std::map<ValueRef, STATS_UNI> CRVPLoad::GetProfValueMap(){
	std::map<ValueRef, STATS_UNI> ProfValueMap;
	for(ANLYS_INFO AI: CLI.AI_VEC){
		if(AI.VAL_TYPE != VAL_IN_DEF || AI.VAL_TYPE != VAL_OUT_DEF){
			STATS_UNI SU;
			if(GetProfData(AI.PROF_TYPE, AI.PROF_ID, SU))
				ProfValueMap[CPI->IdToValRefMap[AI.VAL_ID]] = SU;
		}
	}
	return ProfValueMap;
}

unsigned int CRVPLoad::GetAnlysID(Function* F){
	auto& FM = CPI->FuncToIdMap;
	assert( FM.find(F) != FM.end() && "Cannot find function in FuncToIdMap!");
	return FM[F];
};

unsigned int CRVPLoad::GetAnlysID(BasicBlock* B){
	auto& BM = CPI->BBToIdMap;
	assert( BM.find(B) != BM.end() && "Cannot find basic block in BBToIdMap!");
	return BM[B];
};

unsigned int CRVPLoad::GetAnlysID(Instruction* I){
	auto& VM = CPI->ValToIdMap;
	assert( VM.find(I) != VM.end() && "Cannot find instruction in InstToIdMap!");
	return VM[I];
};

CRVP_PROF_INFO CRVPLoad::GetProfInfo(unsigned int AnlysType, unsigned int AnlysID){
	assert(CLI.valid && "CRVP analysis and profile data are not loaded!");
	
	CRVP_PROF_INFO PI;
	
	PI.AnlysType = AnlysType;
	PI.AnlysID = AnlysID;

	auto& IVM = CPI->IdToValMap;
	auto& IVRM = CPI->IdToValRefMap;

	for(ANLYS_INFO AI: CLI.AI_VEC){
		if(AI.ANLYS_TYPE == AnlysType && AI.ANLYS_ID == AnlysID){
			switch(AI.VAL_TYPE) {
				case(VAL_IN_DEF): 	PI.VAL_IN.push_back(&*IVM[AI.VAL_ID]); break;
				case(VAL_OUT_DEF): 	PI.VAL_OUT.push_back(&*IVM[AI.VAL_ID]); break;
				case(REF_IN_DEF): 	PI.REF_IN.push_back(IVRM[AI.VAL_ID]); break;
				case(REF_OUT_DEF): 	PI.REF_OUT.push_back(IVRM[AI.VAL_ID]); break;
				case(LD_VAL_DEF): 	PI.LD_VAL.push_back(IVRM[AI.VAL_ID]); break;
				case(LD_PTR_DEF): 	PI.LD_PTR.push_back(IVRM[AI.VAL_ID]); break;
				case(ST_VAL_DEF): 	PI.ST_VAL.push_back(IVRM[AI.VAL_ID]); break;
				case(ST_PTR_DEF): 	PI.ST_PTR.push_back(IVRM[AI.VAL_ID]); break;
				default: assert( 0 && "Unknown Profile Type!");
			}
			
			STATS_UNI SU;
			if(GetProfData(AI.PROF_TYPE, AI.PROF_ID, SU)){
				if(AI.VAL_TYPE == VAL_IN_DEF || AI.VAL_TYPE == VAL_OUT_DEF)
					PI.PROF_DATA_MAP[std::make_pair(&*IVM[AI.VAL_ID], nullptr)] = SU;
				else
					PI.PROF_DATA_MAP[IVRM[AI.VAL_ID]] = SU;
			}
		}
	}

	return PI;
};

bool CRVPLoad::DumpProfInfo(unsigned int AnlysType, unsigned AnlysID){
	assert(CLI.valid && "CRVP analysis and profile data are not loaded!");
	
	std::vector<ANLYS_INFO> AI_VEC;
	for(ANLYS_INFO AI: CLI.AI_VEC){
		if(AI.ANLYS_TYPE == AnlysType && AI.ANLYS_ID == AnlysID){
			AI_VEC.push_back(AI);
		}
	}
	
	if(AI_VEC.empty())
		return false;
	
	for(ANLYS_INFO AI : AI_VEC){
		errs() << "\t Value Type: " << AI.VAL_TYPE << ", Value ID: " << AI.VAL_ID << ", ";
		if( AI.VAL_TYPE == VAL_IN_DEF || AI.VAL_TYPE == VAL_OUT_DEF ){
			CPI->IdToValMap[AI.VAL_ID]->print(errs());
		}
		else{
			auto VP = &CPI->IdToValRefMap[AI.VAL_ID];
			VP->first->print(errs());
			errs() << " / ";
			if(Instruction* I = dyn_cast<Instruction>(VP->second))
				I->print(errs());
			else
				errs() << VP->second->getName();
		}
		errs() << ", Profile Type: " << AI.PROF_TYPE << ", Profile ID: " << AI.PROF_ID << "\n";

		STATS_UNI SU;
		GetProfData(AI.PROF_TYPE, AI.PROF_ID, SU);
		if(SU.SB != NULL){
			errs() << "\t\t Bool Type, CNT_1: " << SU.SB->CNT_1 << "; CNT_0: " << SU.SB->CNT_0 << "\n";
			continue;
		}
		if(SU.SI != NULL){
			double EX = (double) SU.SI->SUM / (double) SU.SI->CNT;
			double EX_2 = (double) SU.SI->SUM_2 / (double) SU.SI->CNT;

			errs() << "\t\t Integer Type, CNT: " << SU.SI->CNT << ", MAX: " << SU.SI->MAX << ", MIN: " << SU.SI->MIN 
						 << ", AVG: " << EX
						 << ", STD: " << EX_2 - EX*EX << "\n";
			errs() << "\t\t\t Top 5 Values: ";
			for(int i = 0; i < 5; i ++ )
				errs() << SU.SI->TNV[2*i] << " , " << SU.SI->TNV[2*i+1] << "; ";
			errs() << "\n";
			continue;
		}
		if( SU.SF != NULL){
			double EX = (double) SU.SF->SUM / (double) SU.SF->CNT;
			double EX_2 = (double) SU.SF->SUM_2 / (double) SU.SF->CNT;

			errs() << "\t\t Floating Point Type, CNT: " << SU.SF->CNT << ", MAX: " << SU.SF->MAX << ", MIN: " << SU.SF->MIN 
						 << ", AVG: " << EX
						 << ", STD: " << EX_2 - EX*EX << "\n";
			errs() << "\t\t\t Top 5 Values: ";
			for(int i = 0; i < 5; i ++ )
				errs() << SU.SF->TNV[2*i] << " , " << (int)SU.SF->TNV[2*i+1] << "; ";
			errs() << "\n";
			continue;
		}
	}

	return true;
};

// Load Pass
bool CRVPLoad::runOnModule(Module &M) {
  errs() << "Run CRVPLoad On " << M.getName() << "\n";
	
	CPI = &getAnalysis<CRVPInit>().CPI;
	
	assert(CPI->valid = true);
	LoadCRVPProfData("anlysdata.crvp", "profdata.crvp");
	assert(CLI.valid = true);

	errs() << "Total Value Items: " << CLI.AI_VEC.size() << "\n";
	errs() << "Total Profile Bool Items: " << CLI.SB_VEC.size() << "\n";
	errs() << "Total Profile INT8 Items: " << CLI.SI_VEC[0].size() << "\n";
	errs() << "Total Profile INT16 Items: " << CLI.SI_VEC[1].size() << "\n";
	errs() << "Total Profile INT32 Items: " << CLI.SI_VEC[2].size() << "\n";
	errs() << "Total Profile INT64 Items: " << CLI.SI_VEC[3].size() << "\n";
	errs() << "Total Profile FP32 Items: " << CLI.SF_VEC[0].size() << "\n";
	errs() << "Total Profile FP64 Items: " << CLI.SF_VEC[1].size() << "\n";

	errs() << "Successfully Load CRVP Profile Data!\n";
	return true;
};

void CRVPLoad::getAnalysisUsage(AnalysisUsage &AU) const{
	AU.setPreservesAll();
	AU.addRequired<CRVPInit>();
};

char CRVPLoad::ID = 0;
static RegisterPass<CRVPLoad> CRVPLoad("crvp-load", "Code Region Value Analysis and Profile Data Load.");

// Function Test Pass
bool CRVPFunctionTest::runOnFunction(Function &F){
	errs() << "\n\nRun CRVP Function Test On " << F.getName() << "\n";

	CRVPLoad* CL = &getAnalysis<CRVPLoad>();
	unsigned int AnlysType = FUNC_TYPE_DEF;
	unsigned int AnlysID = CL->GetAnlysID(&F);
	CL->DumpProfInfo(AnlysType, AnlysID);
	
	return true;
};

void CRVPFunctionTest::getAnalysisUsage(AnalysisUsage &AU) const{
	AU.addRequired<CRVPLoad>();
};

char CRVPFunctionTest::ID = 0;
static RegisterPass<CRVPFunctionTest> CRVPFunctionTest("crvp-func-test", "Code Region Value Analysis and Profile Function Data test.");

// Loop Test Pass 
bool CRVPLoopTest::runOnLoop(Loop *L, LPPassManager &LPM){
	errs() << "\n\nRun CRVP Loop Test On " << L->getName() << "\n";
	
	CRVPLoad* CL = &getAnalysis<CRVPLoad>();
	unsigned int AnlysType = LOOP_TYPE_DEF;
	unsigned int AnlysID = CL->GetAnlysID(L->getHeader());	
	CL->DumpProfInfo(AnlysType, AnlysID);
	
	return true;
};

void CRVPLoopTest::getAnalysisUsage(AnalysisUsage &AU) const{
	AU.addRequired<CRVPLoad>();	
};

char CRVPLoopTest::ID = 0;
static RegisterPass<CRVPLoopTest> CRVPLoopTest("crvp-loop-test", "Code Region Value Analysis and Profile Loop Data test.");
