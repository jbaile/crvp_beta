/* 
 * CRVP_Analysis.h
 * Author: Shikai Li
 *
 */

#ifndef CRVP_PROF_H
#define CRVP_PROF_H

//#include <stdio.h>
//#include <fcntl.h>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <string.h>
#include <map>
#include <set>
#include <vector>

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IntrinsicInst.h"

#include "llvm/IR/Constants.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Dominators.h"

#include "llvm/Pass.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/AliasSetTracker.h"
#include "llvm/Analysis/CFG.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Support/raw_ostream.h"

#include "CRVP_Types.h"

using namespace llvm;

#define DEBUG_MSG
//#define PROF_INST_SRC

//#define USER_DEFINE_MODE

// Relaxed region could be any collection of instructions with only 1 entry point
// into the relaxed region, which different from the Region definition in llvm
// which is a collection of basic blocks with only 1 entry block and only
// 1 exit block.
class RelaxedRegion{
  private:
    // All instructions in region
    std::set<Instruction*> InstructionSet;
    // All basic blocks that part of instructions in it is also in region
    std::set<BasicBlock*> BasicBlockSet;
    
    bool isDbgOrLifetime(Instruction *I);
    bool isPHIOrDbgOrLifetime(Instruction *I);
  public:
    // Insert an instruction into the region
    void insertInst(Instruction *I);
    // Insert a basic block into the region
    void insertBlock(BasicBlock *BB);
     
    // Return true if an instruction is in the region
    bool inRelaxedRegion(Instruction *I);
    // Return true if all instructions in the basic block is also in the
    // region
    bool inRelaxedRegion(BasicBlock *BB);
    
    // Return true if the region meet our definition
    bool isLegal();
    
    std::set<Instruction*> getInstructionSet();
    BasicBlock* getEntryBlock();
    Instruction* getEntryInst();
    
    std::vector<std::pair<BasicBlock*,BasicBlock*> > getExitEdges();
    std::vector<Instruction*> getExitInsts();
};

class InstrumentRecord{
  public:
    unsigned int AnlysType;
    unsigned int ValueType;
    
    unsigned int AnlysID;
    unsigned int ValueID;
    
    Value* ProfVal = NULL;
    
    bool InsertBeforePos;
    Instruction* Pos = NULL;

    InstrumentRecord(unsigned int AT, unsigned int VT,
      unsigned int AI, unsigned int VI,
      Value* PE, bool IBP, Instruction* P){
      AnlysType = AT; ValueType = VT;
      AnlysID = AI; ValueID = VI;
      ProfVal = PE; InsertBeforePos = IBP; Pos = P;
    };
};

struct CRVP_INFO{
  bool valid = false;
	bool prof_info_valid = false;
	
	std::vector<InstrumentRecord> InstrumentRecordVec;
  
  unsigned int ValId = 0;
  std::map<Value*, unsigned int> ValToIdMap;
  std::map<unsigned int, Value*> IdToValMap;
  
  unsigned int ValRefId = 0;
  std::map<std::pair<Value*, Value*>, unsigned int > ValRefToIdMap;
  std::map<unsigned int, std::pair<Value*, Value*> > IdToValRefMap;
  
  unsigned int BBID = 0;
  std::map<BasicBlock*, unsigned int> BBToIdMap;
  std::map<unsigned int, BasicBlock*> IdToBBMap;
  
  unsigned int FuncID = 0; 
  std::map<Function*, unsigned int> FuncToIdMap;
  std::map<unsigned int, Function*> IdToFuncMap;
  
  unsigned int RRGNID = 0;
  std::map<RelaxedRegion*, unsigned int> RRGNToIdMap;
  std::map<unsigned int, RelaxedRegion*> IdToRRGNMap;
	
  std::vector<Instruction*>    ProfInstVec;
  std::vector<BasicBlock*>     ProfLoopVec;
  std::vector<RelaxedRegion* > ProfRRgnVec;
  std::vector<Function*>       ProfFuncVec; 

  std::map<BasicBlock*, SmallVector<BasicBlock*, 32> > ProfLoopLatches;
  std::map<BasicBlock*, SmallVector<BasicBlock*, 32> > ProfLoopExitBlocks;
};

namespace llvm {
  struct CRVPInit : public ModulePass {
    public:
      CRVP_INFO CPI;

      static char ID;
      CRVPInit() : ModulePass(ID){};
    	
			void IDGenerate(Module &M);
			void ProfInfoLoad();

      virtual bool runOnModule(Module &M);
      virtual void getAnalysisUsage(AnalysisUsage &AU) const;
  };
  
	struct CRVPAnalysis : public FunctionPass {
    public:
			CRVPInit* CRVPPI;
      CRVP_INFO *CPI;
			  
		 	LoopInfo *LI;
      AliasAnalysis *AA;
      DominatorTree *DT;
      PostDominatorTree *PDT;
      const DataLayout *DL;
      
      static char ID;
      CRVPAnalysis() : FunctionPass(ID){}
			
			bool FunctionValueAnalysisImpl(Function &F, AliasAnalysis *AA, 
				DominatorTree *DT, PostDominatorTree *PDT, const DataLayout *DL);
			bool LoopValueAnalysisImpl(Loop *L, AliasAnalysis *AA, 
				DominatorTree *DT, PostDominatorTree *PDT, const DataLayout *DL);
			bool RelaxedRegionValueAnalysisImpl(RelaxedRegion *R, AliasAnalysis *AA, 
				DominatorTree *DT, PostDominatorTree *PDT, const DataLayout *DL);
			bool InstructionValueAnalysisImpl(Instruction *I);
      
      bool runOnFunction(Function &F);
      void getAnalysisUsage(AnalysisUsage &AU) const;
  };
	
	struct CRVPInstrument : public ModulePass {
    public:
      unsigned int InstrumentID[7] = {};
      
      CRVPInit* CRVPPI;
			CRVP_INFO* CPI;

      Constant* CRVP_MODULE_INIT;
      Constant* CRVP_MODULE_EXIT;
      
      Constant* CRVP_LOOP_BACK;
      Constant* CRVP_LOOP_EXIT;
      
      Constant* CRVP_RRGN_ENTRY;
      Constant* CRVP_RRGN_EXIT;
      
      Constant* CRVP_VAL_INT_1;
      Constant* CRVP_VAL_INT_8;
      Constant* CRVP_VAL_INT_16;
      Constant* CRVP_VAL_INT_32;
      Constant* CRVP_VAL_INT_64;
      Constant* CRVP_VAL_FP_32;
      Constant* CRVP_VAL_FP_64;
       
      static char ID;
      CRVPInstrument() : ModulePass(ID){};
      
      void createDeclarations(Module &M);
      bool InstrumentValProfCall(InstrumentRecord &IR, Module &M, unsigned int& ValueType);

      virtual bool runOnModule(Module &M);
      void getAnalysisUsage(AnalysisUsage &AU) const;
  };
  
}
#endif // CRVP_PROF_H
