/* 
 * crvp_types.h
 * Author: Shikai Li, CCCP Group, Umich
 *
 */
#ifndef CRVP_TYPE_H
#define CRVP_TYPE_H

#define NUM_STEADY 5
#define NUM_CLEAR  10

#include <stdint.h>

#define INT1_TYPE_DEF 	0
#define INT8_TYPE_DEF 	1
#define INT16_TYPE_DEF 	2
#define INT32_TYPE_DEF 	3
#define INT64_TYPE_DEF 	4
#define FP32_TYPE_DEF 	5
#define FP64_TYPE_DEF 	6
#define CONST_TYPE_DEF 	7
#define NONE_TYPE_DEF 	8

#define FUNC_TYPE_DEF 	0
#define LOOP_TYPE_DEF 	1
#define INST_TYPE_DEF   2
#define RRGN_TYPE_DEF 	3

#define VAL_IN_DEF 			0
#define VAL_OUT_DEF 		1
#define REF_IN_DEF  		2
#define REF_OUT_DEF 		3
#define LD_PTR_DEF 			4
#define LD_VAL_DEF 			5
#define ST_PTR_DEF 			6
#define ST_VAL_DEF 			7

// Internal Data Structure for Loading Analysis Result at Profiling
struct ANLYS_SUM{
 uint32_t ANLYS_SIZE;
 uint32_t PROF_SIZE[7];
};

// Analysis Infomation Structure
// 	ANLYS_TYPE and ANLYS_ID are used to index the analysis in which the target
// 	value is generated;
// 	VAL_TYPE and VAL_ID, together with ANLYS_TYPE andANLYS_ID, are used to index
// 	the target value;
// 	PROF_TYPE and PROF_ID are used to index the corresponding profile value.
// 	Since different target values in different analysises might be the same, in
// 	this case they would be combined as one profile value in profiling stage.
struct ANLYS_INFO{
  uint32_t ANLYS_TYPE;
  uint32_t ANLYS_ID;
  uint32_t VAL_TYPE;
  uint32_t VAL_ID;
  uint32_t PROF_TYPE;
  uint32_t PROF_ID;
};

// Profile Data Structure For Boolean Type
// **CNT_0 -- the time the value takes false
// **CNT_1 -- the time the value takes true
struct STATS_BOOL{
 uint32_t CNT_0;
 uint32_t CNT_1;
};

// Internal Data Structure at Runtime
struct STATS_INT_TNV{
 uint64_t TNV[2*(NUM_STEADY+NUM_CLEAR)];
 
 int tic;
 uint64_t last_value;
 int num_profiled;
 int pos_lfe_steady;
 int pos_lfe_clear;
};

// Profile Data Structure For Integer Type
// **CNT -- the time the value occurs
// **CNT_0 -- the time the value takes 0
// **MAX, MIN -- the max/min for signed integer
// **SUM, SUM_2 -- the sum/(sum of all squares) for signed integer
// **UMAX, UMIN -- the max/min for unsigned integer
// **USUM, USUM_2 -- the sum/(sum of all squares) for unsigned integer
// **TNV -- the array contains top 5 values
// 		 			TNV[2*i] is the top i+1 value
// 					TNV[2*i+1] is the time that top i+1 value occurs
struct STATS_INT{
 uint32_t CNT;
 uint32_t CNT_0;
 
 int64_t MAX;
 int64_t MIN;
 __int128_t SUM;
 __int128_t SUM_2;
 
 uint64_t UMAX;
 uint64_t UMIN;
 __uint128_t USUM;
 __uint128_t USUM_2;

 uint64_t TNV[2*(NUM_STEADY)];
 struct STATS_INT_TNV* TNV_PTR;
};

// Internal Data Structure at Runtime
struct STATS_FP_TNV{
 double TNV[2*(NUM_STEADY+NUM_CLEAR)];
 
 int tic;
 double last_value;
 int num_profiled;
 int pos_lfe_steady;
 int pos_lfe_clear;
};

// Profile Data Structure For Floating Point Type
// **CNT -- the time the value occurs
// **MAX, MIN -- the max/min
// **SUM, SUM_2 -- the sum/(sum of all squares)
// **TNV -- the array contains top 5 values
// 		 			TNV[2*i] is the top i+1 value
// 					TNV[2*i+1] is the time that top i+1 value occurs
struct STATS_FP{
 uint32_t CNT;
 
 double MAX;
 double MIN;
 double SUM;
 double SUM_2;

 /*
 double _SUM_A;
 double _SUM_A_2;
 double _SUM_B;
 double _SUM_B_2;
 */

 double TNV[2*(NUM_STEADY)];
 struct STATS_FP_TNV* TNV_PTR;
};


// Universal Profile Data Structure
// 	One and only one pointer should be NONNULL, which points to profiling data
// 	and indicates the data type.
struct STATS_UNI{
  struct STATS_BOOL*   SB;
  struct STATS_INT*    SI;
  struct STATS_FP*     SF;
};

#endif //CRVP_TYPE_H
