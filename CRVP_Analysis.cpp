/* 
 * CRVP_Analysis.cpp
 * Author: Shikai Li
 *
 */

#include "CRVP_Analysis.h"
#include <assert.h>

using namespace llvm;
// ***************************//
// ****** Gernal Tools *******//
// ***************************//
int GetIndexInBlock(Instruction *I){
  BasicBlock *BB = I->getParent();
  int index = -1;
  for(BasicBlock::iterator BI = BB->begin(), BE = BB->end(); BI != BE; ++BI){
    Instruction *II = &*BI;
    index++;
    if(II == I) break;
  }
  return index;
}

void SafeInsertBefore(Instruction *I, Instruction *Pos){
  BasicBlock *BB = Pos->getParent();
  Instruction* SafePos = &*BB->getFirstInsertionPt();

  int pos_dis = GetIndexInBlock(Pos) - GetIndexInBlock(SafePos);
  if(pos_dis > 0)
    I->insertBefore(Pos);
  else
    I->insertBefore(SafePos);
}

void SafeInsertAfter(Instruction *I, Instruction *Pos){
  BasicBlock *BB = Pos->getParent();
  Instruction* SafePos = &*BB->getFirstInsertionPt();
  Instruction* TermPos = &*BB->getTerminator();

  if(isa<ReturnInst>(Pos)) { I->insertBefore(Pos); return;}
  
  int pos_dis = GetIndexInBlock(Pos) - GetIndexInBlock(SafePos);
  int term_dis = GetIndexInBlock(Pos) - GetIndexInBlock(TermPos);
  if(pos_dis >= 0 && term_dis < 0)
    I->insertAfter(Pos);
  else if(pos_dis < 0 && term_dis < 0)
    I->insertBefore(SafePos);
  else
    I->insertBefore(TermPos);
}

void SafeInsert(Instruction* I, bool InsertBeforePos, Instruction* Pos){
  if(InsertBeforePos)
    SafeInsertBefore(I, Pos);
  else
    SafeInsertAfter(I, Pos);
}

// ******************************************//
// *** RelaxedRegion Class Implementation ***//
// ******************************************//
// TODO: Handle PHI Node Problem in RelaxedRegion
bool RelaxedRegion::isDbgOrLifetime(Instruction *I){
  if(isa<DbgInfoIntrinsic>(*I))
    return true;

  if(auto *II = dyn_cast<IntrinsicInst>(I))
    if(II->getIntrinsicID() == Intrinsic::lifetime_start ||
      II->getIntrinsicID() == Intrinsic::lifetime_end)
      return true;

  return false;
}

bool RelaxedRegion::isPHIOrDbgOrLifetime(Instruction *I){
  if(isa<PHINode>(*I))
    return true; 
  return isDbgOrLifetime(I);
}

void RelaxedRegion::insertInst(Instruction* I){
  if(isPHIOrDbgOrLifetime(I))
    return;
  InstructionSet.insert(I);
  BasicBlockSet.insert(I->getParent());
}

void RelaxedRegion::insertBlock(BasicBlock* B){
  for(BasicBlock::iterator BI = B->begin(), BE = B->end(); BI != BE; ++BI){
    Instruction* I = &*BI;
    if(isPHIOrDbgOrLifetime(I))
      continue;
    InstructionSet.insert(I);
  }
  BasicBlockSet.insert(B);
}

bool RelaxedRegion::inRelaxedRegion(Instruction *I){
  return InstructionSet.find(I) != InstructionSet.end();
}

bool RelaxedRegion::inRelaxedRegion(BasicBlock *B){
  return BasicBlockSet.find(B) != BasicBlockSet.end();
}

std::set<Instruction*> RelaxedRegion::getInstructionSet(){
  assert(isLegal() && "RelaxedRegion is ilegal to be used!\n");
  return InstructionSet;
}

// FIXME:Currently Only Consider Predecessors
bool RelaxedRegion::isLegal(){
  std::set<BasicBlock*> outRelaxedRegionPred;
  //std::set<BasicBlock*> outRelaxedRegionSucc;

  for(BasicBlock* B : BasicBlockSet){
    for(auto it = pred_begin(B), et = pred_end(B); it != et; ++it){
      BasicBlock* Pred = *it;
      if(!inRelaxedRegion(Pred))
        outRelaxedRegionPred.insert(Pred);
    }
    /*
    for(auto it = succ_begin(B), et = succ_end(B); it != et; ++it){
      BasicBlock* Succ = *it;
      if(!inRelaxedRegion(Succ))
        outRelaxedRegionSucc.insert(Succ);
    }
    */
  }

  if(outRelaxedRegionPred.size() != 1)
    return false;
  
  return true;
}

BasicBlock* RelaxedRegion::getEntryBlock(){
  assert(isLegal() && "RelaxedRegion is ilegal to be used!\n");
  
  for(BasicBlock* B : BasicBlockSet){
    for(auto it = pred_begin(B), et = pred_end(B); it != et; ++it){
      BasicBlock* Pred = *it;
      if(!inRelaxedRegion(Pred)) return B;
    }
  }

  return NULL;
}

Instruction* RelaxedRegion::getEntryInst(){
  BasicBlock* EntryBlock = getEntryBlock();
  
  for(BasicBlock::iterator BI = EntryBlock->begin(), BE = EntryBlock->end(); BI != BE; ++BI){
    Instruction* I = &*BI;
    if(inRelaxedRegion(I)) return I;
  }
  
  return NULL;
}

std::vector<std::pair<BasicBlock*,BasicBlock*> > RelaxedRegion::getExitEdges(){
  assert(isLegal() && "RelaxedRegion is ilegal to be used!\n");
  
  std::vector<std::pair<BasicBlock*,BasicBlock*> > ExitEdges;
  for(BasicBlock* B : BasicBlockSet){
    for(auto it = succ_begin(B), et = succ_end(B); it != et; ++it){
      BasicBlock* Succ = *it;
      if(!inRelaxedRegion(Succ))
        ExitEdges.push_back(std::make_pair(B,Succ));
    }
  }

  return ExitEdges;
}

std::vector<Instruction*> RelaxedRegion::getExitInsts(){
  std::vector<Instruction*> ExitInsts;
  std::vector<std::pair<BasicBlock*,BasicBlock*> > ExitEdges = getExitEdges();
  
  for(auto Edge : ExitEdges){
    BasicBlock::iterator BI = Edge.first->begin();
    while(&*BI != Edge.first->getFirstNonPHIOrDbgOrLifetime()) ++BI;
    for(BasicBlock::iterator BE = Edge.first->end(); BI != BE; ++BI){
      Instruction* I = &*BI;
      if(!inRelaxedRegion(I)){
        ExitInsts.push_back(I);
        continue;
      }
    }
    Instruction* I = &*Edge.second->getFirstNonPHIOrDbgOrLifetime();
    ExitInsts.push_back(I);
  }

  return ExitInsts;
}

// *** Pass Trace:
// CRVPInit -> CRVPAnalysis -> CRVPInstrument

// *******************************************//
// *** Code Region Value Profile Init Pass ***//
// *******************************************//
void CRVPInit::IDGenerate(Module &M){
	if(CPI.valid) return;
	
  for(Module::iterator MI = M.begin(), ME = M.end(); MI != ME; ++MI){
    Function& F = *MI;
    CPI.IdToFuncMap[CPI.FuncID] = &F;
    CPI.FuncToIdMap[&F] = CPI.FuncID++;
    
    for(Function::arg_iterator AI = F.arg_begin(), AE = F.arg_end(); AI != AE; ++AI){
      Value& V = *AI;
      if(CPI.ValToIdMap.find(&V) == CPI.ValToIdMap.end()){
        CPI.IdToValMap[CPI.ValId] = &V;
        CPI.ValToIdMap[&V] = CPI.ValId++;
      }
      CPI.IdToValRefMap[CPI.ValRefId] = std::make_pair(&V, &F);
      CPI.ValRefToIdMap[std::make_pair(&V, &F)] = CPI.ValRefId++;
    }
    
    for(Function::iterator FI = F.begin(), FE = F.end(); FI != FE; ++FI){
      BasicBlock& B = *FI;
      CPI.IdToBBMap[CPI.BBID] = &B;
      CPI.BBToIdMap[&B] = CPI.BBID++;
      
      for(BasicBlock::iterator BI = B.begin(), BE = B.end(); BI != BE; ++BI){
        Instruction& I = *BI;
        if( CPI.ValToIdMap.find(&I) == CPI.ValToIdMap.end()){
          CPI.IdToValMap[CPI.ValId] = &I;
          CPI.ValToIdMap[&I] = CPI.ValId++;
        }
        CPI.IdToValRefMap[CPI.ValRefId] = std::make_pair(&I, &I);
        CPI.ValRefToIdMap[std::make_pair(&I, &I)] = CPI.ValRefId++;
        
        for(Value* OP : I.operands()){
          if(CPI.ValToIdMap.find(OP) == CPI.ValToIdMap.end()){
            CPI.IdToValMap[CPI.ValId] = OP;
            CPI.ValToIdMap[OP] = CPI.ValId++;
          }
          CPI.IdToValRefMap[CPI.ValRefId] = std::make_pair(OP, &I);
          CPI.ValRefToIdMap[std::make_pair(OP, &I)] = CPI.ValRefId++;
        }
      }
    }
  }
	
	CPI.valid = true;
}

void CRVPInit::ProfInfoLoad(){
	if(CPI.prof_info_valid) return;

	std::ifstream Profile;
	Profile.open("Profile.txt", std::ios::in);
	assert(Profile && "User defined profile infomation file not found!\n");
	unsigned int AnlysTy;
	unsigned int AnlysID;
	while(Profile >> AnlysTy >> AnlysID){
		switch(AnlysTy) {
			case FUNC_TYPE_DEF:{
				CPI.ProfFuncVec.push_back(CPI.IdToFuncMap[AnlysID]);
				break;}
			case LOOP_TYPE_DEF:{
				CPI.ProfLoopVec.push_back(CPI.IdToBBMap[AnlysID]);
				break;}
			case INST_TYPE_DEF:{
				Instruction *I = dyn_cast<Instruction>(CPI.IdToValMap[AnlysID]);
				CPI.ProfInstVec.push_back(I);
				break;}
			default:
				assert( 0 && "Undefined analysis type!");
		}
	}
	Profile.close();

	CPI.prof_info_valid = true;
}

bool CRVPInit::runOnModule(Module &M) {
	#ifdef DEBUG_MSG
  errs() << "Run CRVPInit On " << M.getName() << ";\n";
  #endif
  
	IDGenerate(M);
	//ProfInfoLoad();

	return true;
}

void CRVPInit::getAnalysisUsage(AnalysisUsage &AU) const{
	AU.setPreservesAll();
}

char CRVPInit::ID = 0;
static RegisterPass<CRVPInit> CRVPInit("crvp-init", "Code Region Value Profile Init");

// ****************************************//
// *** Load Store Adress Alias Analysis ***//
// ****************************************//
bool LdStAliasAnalysis(std::vector<LoadInst*>& LoadInstVec,
                       std::vector<StoreInst*>& StoreInstVec,
                       AliasAnalysis* AA, const DataLayout* DL, 
                       DominatorTree* DT, PostDominatorTree* PDT,
                       std::vector<LoadInst*>& NonBlockedLoadInstVec,
                       std::vector<StoreInst*>& NonBlockedStoreInstVec){

  for(LoadInst* LI : LoadInstVec){
    uint64_t LSize = 0;
    if(LI->getType()->isSized())
      LSize = DL->getTypeStoreSize(LI->getType());
    bool MustLoadFromLocal = false;
    for(StoreInst* SI: StoreInstVec){
      uint64_t SSize = 0;
      if(SI->getType()->isSized())
        SSize = DL->getTypeStoreSize(SI->getType());
      AliasResult Alias = AA->alias(LI->getOperand(0), LSize, SI->getOperand(1), SSize);
      if(Alias == MustAlias){
        if(LI->getParent() == SI->getParent()){
          if(GetIndexInBlock(SI) < GetIndexInBlock(LI))
            MustLoadFromLocal = true;
        }
        else{
          if(DT->dominates(SI, LI))
            MustLoadFromLocal = true;
        }
      }
    }
    if(!MustLoadFromLocal)
      NonBlockedLoadInstVec.push_back(LI);
  }
  
  for(StoreInst* SI0: StoreInstVec){
    uint64_t SSize0 = 0;
    if(SI0->getType()->isSized())
      SSize0 = DL->getTypeStoreSize(SI0->getType());            
    bool MustStoreToLocal = false;
    for(StoreInst* SI1: StoreInstVec){
      uint64_t SSize1 = 0;
      if(SI1->getType()->isSized())
        SSize1 = DL->getTypeStoreSize(SI1->getType());
      AliasResult Alias = AA->alias(SI0->getOperand(1), SSize0, SI1->getOperand(1), SSize1);
      if(Alias == MustAlias){
        if(SI0->getParent() == SI1->getParent()){
          if(GetIndexInBlock(SI0) < GetIndexInBlock(SI1))
            MustStoreToLocal = true;
        }
        else{
          if(PDT->dominates(SI1->getParent(), SI0->getParent()))
            MustStoreToLocal = true;
        }
      }
    }
    if(!MustStoreToLocal)
      NonBlockedStoreInstVec.push_back(SI0);
  }

  return true;
}

// *****************************************//
// *** Load Store Address Scope Analysis ***//
// *****************************************//
// WARNING: All the analysis bellow is based on the assumption that different
// stack allocation will NOT overlap, and all stack access will NOT overflow
void GetAllocaInstVec(Function* F, std::vector<AllocaInst*>& AllocaInstVec){
  for(Function::iterator FI = F->begin(), FE = F->end(); FI != FE; ++FI){
    BasicBlock* BB = &*FI;
    BasicBlock::iterator BI = BB->begin();
    while(&*BI != BB->getFirstNonPHIOrDbgOrLifetime()) ++BI;
    for(BasicBlock::iterator BE = BB->end(); BI != BE; ++BI){
    //for(BasicBlock::iterator BI = BB->begin(), BE = BB->end(); BI != BE; ++BI){
      Instruction* I = &*BI;
      if(AllocaInst* A = dyn_cast<AllocaInst>(I)){
        AllocaInstVec.push_back(A);
      }
    }
  }
  return;
}

// TODO: What if the consumer is a stroe instruction or a function call
void FindConsumerSet(Value* V, std::set<Value*>& ConsumerSet){
  for(User* U: V->users()){
    if(ConsumerSet.find(U) != ConsumerSet.end()){
      ConsumerSet.insert(U);
      FindConsumerSet(U, ConsumerSet);
    }
  }
}

void FindShiftedAddrSet(Instruction* Addr, std::set<Instruction*>& ShiftedAddrSet){
 ShiftedAddrSet.insert(Addr);

  for(User* U: Addr->users()){
    if(Instruction* I = dyn_cast<Instruction>(U)){
      if(ShiftedAddrSet.find(I) == ShiftedAddrSet.end()){
        if(I->isBinaryOp()){
          FindShiftedAddrSet(I, ShiftedAddrSet);
        }
        else if(LoadInst* LI = dyn_cast<LoadInst>(I)){
          if(Addr == LI->getPointerOperand())
            ShiftedAddrSet.insert(I); 
        }
        else if(StoreInst* SI = dyn_cast<StoreInst>(I)){
          if(Addr == SI->getPointerOperand())
            ShiftedAddrSet.insert(I); 
        }
      }
    }
  }
}

void CheckLdStUseShiftedStackAddr(Instruction* I, std::set<Instruction*>& ShiftedStackAddrSet,
                                std::vector<LoadInst*>& LoadInstVec, std::vector<StoreInst*>& StoreInstVec){
  if(LoadInst* LI = dyn_cast<LoadInst>(I)){
    if(Instruction* Loc = dyn_cast<Instruction>(LI->getPointerOperand())){
      if(ShiftedStackAddrSet.find(Loc) == ShiftedStackAddrSet.end())
        LoadInstVec.push_back(LI);
    }
    else{
        LoadInstVec.push_back(LI);
    }
  }
  else if(StoreInst * SI = dyn_cast<StoreInst>(I)){ 
    if(Instruction* Loc = dyn_cast<Instruction>(SI->getPointerOperand())){
      if(ShiftedStackAddrSet.find(Loc) == ShiftedStackAddrSet.end())
        StoreInstVec.push_back(SI);
    }
    else{
        StoreInstVec.push_back(SI);
    }
  }
}

// ** For function, it substracts all the load/store instructions NOT
// perfromed on the stack address.
bool LdStScopeAnalysis(Function* F, std::vector<LoadInst*>& LoadInstVec, std::vector<StoreInst*>& StoreInstVec){
  // Find ALL stack address and shifted stakck address, which is the
  // allocation instructions inside the function and the binary operation
  // instruction chains from allocation instruction to pointer operand.
  std::vector<AllocaInst*> StackAllocaInstVec;
  std::set<Instruction*> ShiftedStackAddrSet;
  
  GetAllocaInstVec(F, StackAllocaInstVec);
  for(AllocaInst* AI : StackAllocaInstVec)
    FindShiftedAddrSet(AI, ShiftedStackAddrSet);
  
  // Find ALL load/store instructions which is not performed on shifted stack
  // address.
  for(Function::iterator FI = F->begin(), FE = F->end(); FI != FE; ++FI){
    BasicBlock* BB = &*FI;
    BasicBlock::iterator BI = BB->begin();
    while(&*BI != BB->getFirstNonPHIOrDbgOrLifetime()) ++BI;
    for(BasicBlock::iterator BE = BB->end(); BI != BE; ++BI){
      Instruction* I = &*BI;
      CheckLdStUseShiftedStackAddr(I, ShiftedStackAddrSet, LoadInstVec, StoreInstVec);
    }
  }
}

// *** For loop, it regard all the allocation instructions which is used only
// inside the loop as 
bool LdStScopeAnalysis(Loop* L, std::vector<LoadInst*>& LoadInstVec, std::vector<StoreInst*>& StoreInstVec){
  // Find ALL allocation instructions in the function. To be conservative, if
  // the allocation instruction has all consumers inside the loop, then the
  // allocation instruction could be regard as the stack of the loop.
  Function* F = L->getHeader()->getParent();
  
  std::vector<AllocaInst*> StackAllocaInstVec;
  std::vector<AllocaInst*> InRangeStackAllocaInstVec;
  std::set<Instruction*> ShiftedInRangeStackAddrSet;
  
  GetAllocaInstVec(F, StackAllocaInstVec);
  for(AllocaInst* AI : StackAllocaInstVec){
    std::set<Value*> StackConsumerSet;      
    FindConsumerSet(AI, StackConsumerSet);
    
    bool StackInLoopScope = true;
    for(Value* V: StackConsumerSet){
      if(Instruction* I = dyn_cast<Instruction>(V)){
        if(!L->contains(I)){
          StackInLoopScope = false;
          break;
        }
        if(CallInst* CI = dyn_cast<CallInst>(I)){
          StackInLoopScope = false;
          break;
        }
      }
    }
    
    if(StackInLoopScope){
      InRangeStackAllocaInstVec.push_back(AI);
      FindShiftedAddrSet(AI, ShiftedInRangeStackAddrSet);
    }
  }
  
  for(Loop::block_iterator LI = L->block_begin(), LE = L->block_end(); LI != LE; ++LI){
    BasicBlock* BB = *LI;
    BasicBlock::iterator BI = BB->begin();
    while(&*BI != BB->getFirstNonPHIOrDbgOrLifetime()) ++BI;
    for(BasicBlock::iterator BE = BB->end(); BI != BE; ++BI){
      Instruction* I = &*BI;
      CheckLdStUseShiftedStackAddr(I, ShiftedInRangeStackAddrSet, LoadInstVec, StoreInstVec);
    }
  }
	
	return true;
}

bool LdStScopeAnalysis(RelaxedRegion* R, std::vector<LoadInst*>& LoadInstVec, std::vector<StoreInst*>& StoreInstVec){
  // Find ALL allocation instructions in the function. To be conservative, if
  // the allocation instruction has all consumers inside the relaxed region,
  // then the allocation instruction could be regard as the stack of the
  // relaxed region.
  Function* F = R->getEntryBlock()->getParent();
  
  std::vector<AllocaInst*> StackAllocaInstVec;
  std::vector<AllocaInst*> InRangeStackAllocaInstVec;
  std::set<Instruction*> ShiftedInRangeStackAddrSet;
  
  GetAllocaInstVec(F, StackAllocaInstVec);
  for(AllocaInst* AI : StackAllocaInstVec){
    std::set<Value*> StackConsumerSet;      
    FindConsumerSet(AI, StackConsumerSet);
    
    bool StackInRelaxedRegionScope = true;
    for(Value* V: StackConsumerSet){
      if(Instruction* I = dyn_cast<Instruction>(V)){
        if(!R->inRelaxedRegion(I)){
          StackInRelaxedRegionScope = false;
          break;
        }
        if(CallInst* CI = dyn_cast<CallInst>(I)){
          StackInRelaxedRegionScope = false;
          break;
        }
      }
    } 
    
    if(StackInRelaxedRegionScope){
      InRangeStackAllocaInstVec.push_back(AI);
      FindShiftedAddrSet(AI, ShiftedInRangeStackAddrSet);
    }
  }
  
  for(Instruction *I : R->getInstructionSet()){
    CheckLdStUseShiftedStackAddr(I, ShiftedInRangeStackAddrSet, LoadInstVec, StoreInstVec);
  }

	return true;
}

// ************************************//
// *** Function Value Analysis Pass ***//
// ************************************//
bool CRVPAnalysis::FunctionValueAnalysisImpl(Function &F, AliasAnalysis *AA, 
	DominatorTree *DT, PostDominatorTree *PDT, const DataLayout *DL) {
 
	#ifdef DEBUG_MSG
  errs() << "\tRun FunctionValueAnalysis On " << F.getName() << ";\n";
	#endif
	
	assert(&F != nullptr && AA != nullptr && DT != nullptr && PDT != nullptr && DL != nullptr);
	
	std::vector<LoadInst*> LoadInstVec;
  std::vector<StoreInst*> StoreInstVec;
  LdStScopeAnalysis(&F, LoadInstVec, StoreInstVec);
 	
  std::vector<LoadInst*> ExternLoadInstVec;
  std::vector<StoreInst*> ExternStoreInstVec;
  LdStAliasAnalysis(LoadInstVec, StoreInstVec, AA, DL, DT, PDT, 
                    ExternLoadInstVec, ExternStoreInstVec);
  
  unsigned int CurFuncID = CPI->FuncToIdMap[&F];
  for(LoadInst* LI : ExternLoadInstVec){
    Value* Ptr = LI->getPointerOperand();
    Value* Val = LI;

    InstrumentRecord IR_PTR(FUNC_TYPE_DEF, LD_PTR_DEF, CurFuncID,
      CPI->ValRefToIdMap[std::make_pair(Ptr, LI)], Ptr, true, LI);
    CPI->InstrumentRecordVec.push_back(IR_PTR);

    InstrumentRecord IR_VAL(FUNC_TYPE_DEF, LD_VAL_DEF, CurFuncID,
      CPI->ValRefToIdMap[std::make_pair(LI, LI)], Val, false, LI);
    CPI->InstrumentRecordVec.push_back(IR_VAL);
  }
  
  for(StoreInst* SI : ExternStoreInstVec){
    Value* Ptr = SI->getPointerOperand();
    Value* Val = SI->getValueOperand();
    
    InstrumentRecord IR_PTR(FUNC_TYPE_DEF, ST_PTR_DEF, CurFuncID,
      CPI->ValRefToIdMap[std::make_pair(Ptr, SI)], Ptr, true, SI);
    CPI->InstrumentRecordVec.push_back(IR_PTR);
    
    InstrumentRecord IR_VAL(FUNC_TYPE_DEF, ST_VAL_DEF, CurFuncID,
      CPI->ValRefToIdMap[std::make_pair(Val, SI)], Val, true, SI);
    CPI->InstrumentRecordVec.push_back(IR_VAL);
  }
  
  for(Function::arg_iterator I = F.arg_begin(), E = F.arg_end(); I != E; I++){
    Value* V = &*I;
    Instruction* RefLoc = &*F.getEntryBlock().getFirstInsertionPt();
    InstrumentRecord IR(FUNC_TYPE_DEF, REF_IN_DEF, CurFuncID,
      CPI->ValRefToIdMap[std::make_pair(V, &F)], V, false, RefLoc);
    CPI->InstrumentRecordVec.push_back(IR);
  }
  
  for(Function::iterator IF = F.begin(), EF = F.end(); IF != EF; IF++){
    BasicBlock* BB = &*IF;
    BasicBlock::iterator BI = BB->begin();
    while(&*BI != BB->getFirstNonPHIOrDbgOrLifetime()) ++BI;
    for(BasicBlock::iterator BE = BB->end(); BI != BE; ++BI){
      Instruction* I = &*BI;
      if(isa<ReturnInst>(I)){
        InstrumentRecord IR(FUNC_TYPE_DEF, REF_OUT_DEF, CurFuncID,
          CPI->ValRefToIdMap[std::make_pair(I, I)], I, true, I);
        CPI->InstrumentRecordVec.push_back(IR);
      }
    }
  }
	 
  return true;
}

// ********************************//
// *** Loop Value Analysis Pass ***//
// ********************************//
void LoopInputOutputValueAnalysis(Loop *L, std::set<Value*>& ValInSet, std::set<Value*>& ValOutSet){
  // TODO: No explicit methodology to handle function calls inside loops.
  for(Loop::block_iterator LBI = L->block_begin(), LBE = L->block_end(); LBI != LBE; ++LBI){
    BasicBlock *BB = *LBI;
    BasicBlock::iterator BI = BB->begin();
    while(&*BI != BB->getFirstNonPHIOrDbgOrLifetime()) ++BI;
    for(BasicBlock::iterator BE = BB->end(); BI != BE; ++BI){
      
      Instruction *I = &*BI;
      for(Value *O: I->operands()){
        if(Instruction *OI = dyn_cast<Instruction>(O)){
          if(isa<AllocaInst>(OI))
            continue;
          if(!L->contains(OI->getParent()))
            ValInSet.insert(O);
        }
				else if(Argument *OA = dyn_cast<Argument>(O)){
            ValInSet.insert(O);
				}
      }
      
      for(User* U: I->users()){
        if(Instruction *UI = dyn_cast<Instruction>(U)){
          if(isa<ReturnInst>(UI)){
            ValOutSet.insert(I);
            break; 
          }
          if(!L->contains(UI->getParent())){
            ValOutSet.insert(I);
            break;
          }
        }
      }
    }
  }
}

bool CRVPAnalysis::LoopValueAnalysisImpl(Loop *L, AliasAnalysis *AA,
	DominatorTree *DT, PostDominatorTree *PDT, const DataLayout *DL) {
  
	#ifdef DEBUG_MSG
  errs() << "\tRun LoopValueAnalysis On " << L->getName() << ";\n"; 
  #endif
	
  SmallVector<BasicBlock*, 32> Latches;
  L->getLoopLatches(Latches);
  CPI->ProfLoopLatches[L->getHeader()] = Latches;
  
  SmallVector<BasicBlock*, 32> ExitBlocks;
  L->getExitBlocks(ExitBlocks);
  CPI->ProfLoopExitBlocks[L->getHeader()] = ExitBlocks;
  
  Function* F = L->getHeader()->getParent();
  
	std::vector<LoadInst*> LoadInstVec;
  std::vector<StoreInst*> StoreInstVec;
  LdStScopeAnalysis(L, LoadInstVec, StoreInstVec);
  
  std::vector<LoadInst*> ExternLoadInstVec;
  std::vector<StoreInst*> ExternStoreInstVec;
  LdStAliasAnalysis(LoadInstVec, StoreInstVec, AA, DL, DT, PDT, 
                    ExternLoadInstVec, ExternStoreInstVec);
 
  unsigned int CurLoopID = CPI->BBToIdMap[L->getHeader()];
  for(LoadInst* LI : ExternLoadInstVec){
    Value* Ptr = LI->getPointerOperand();
    Value* Val = LI;
    
    InstrumentRecord IR_PTR(LOOP_TYPE_DEF, LD_PTR_DEF, CurLoopID,
      CPI->ValRefToIdMap[std::make_pair(Ptr, LI)], Ptr, true, LI);
    CPI->InstrumentRecordVec.push_back(IR_PTR);
    
    InstrumentRecord IR_VAL(LOOP_TYPE_DEF, LD_VAL_DEF, CurLoopID,
      CPI->ValRefToIdMap[std::make_pair(LI, LI)], Val, false, LI);
    CPI->InstrumentRecordVec.push_back(IR_VAL);
  }
  
  for(StoreInst* SI : ExternStoreInstVec){
    Value* Ptr = SI->getPointerOperand();
    Value* Val = SI->getValueOperand();
    
    InstrumentRecord IR_PTR(LOOP_TYPE_DEF, ST_PTR_DEF, CurLoopID,
      CPI->ValRefToIdMap[std::make_pair(Ptr, SI)], Ptr, true, SI);
    CPI->InstrumentRecordVec.push_back(IR_PTR);
    
    InstrumentRecord IR_VAL(LOOP_TYPE_DEF, ST_VAL_DEF, CurLoopID,
      CPI->ValRefToIdMap[std::make_pair(Val, SI)], Val, true, SI);
    CPI->InstrumentRecordVec.push_back(IR_VAL);
  }
  
  std::set<Value*> VarInSet;
  std::set<Value*> VarOutSet;
  LoopInputOutputValueAnalysis(L, VarInSet, VarOutSet);

  for(Value* VI: VarInSet){
		Instruction* RefLoc = &*L->getHeader()->getFirstInsertionPt();
    InstrumentRecord IR(LOOP_TYPE_DEF, VAL_IN_DEF, CurLoopID,
      CPI->ValToIdMap[VI], VI, false, RefLoc);
    CPI->InstrumentRecordVec.push_back(IR);
  }
  
  for(Value* VO: VarOutSet){
    if(Instruction* VOI = dyn_cast<Instruction>(VO)){
      InstrumentRecord IR(LOOP_TYPE_DEF, REF_OUT_DEF, CurLoopID,
        CPI->ValRefToIdMap[std::make_pair(VOI, VOI)], VOI, false, VOI);
      CPI->InstrumentRecordVec.push_back(IR);
    }
    else{
      assert( 0 && "No output value of loop could be llvm::Value type!\n");
    }
  }

  return true;
}

// *****************************************//
// *** RelaxedRegion Value Analysis Pass ***//
// *****************************************//
void RelaxedRegionInputOutputValueAnalysis(RelaxedRegion *R, std::set<Value*>& ValInSet, std::set<Value*>& ValOutSet){
  for(Instruction* I : R->getInstructionSet()){
    for(Value *O: I->operands()){
      if(Instruction *OI = dyn_cast<Instruction>(O)){
       	if(isa<AllocaInst>(OI))
        	continue;
        if(!R->inRelaxedRegion(OI))
          ValInSet.insert(O);
      } else if(Argument *OA = dyn_cast<Argument>(O)){
				ValInSet.insert(O);
			}
    }
    
    for(User* U: I->users()){
      if(Instruction *UI = dyn_cast<Instruction>(U)){
        if(isa<ReturnInst>(UI)){
          ValOutSet.insert(I);
          break; 
        }
        if(!R->inRelaxedRegion(UI)){
          ValOutSet.insert(I);
          break;
        }
      }
    }
  }
}

bool CRVPAnalysis::RelaxedRegionValueAnalysisImpl(RelaxedRegion *R, AliasAnalysis *AA,
	DominatorTree *DT, PostDominatorTree *PDT, const DataLayout* DL) {

  #ifdef DEBUG_MSG
	errs() << "\tRun RelaxedRegionValueAnalysis On " << R->getEntryBlock()->getName() << ";\n";
  #endif
	
	errs() << "\tRelaxed Region Value Analysis is not supported at current version.";
	return false;
	
	std::vector<LoadInst*> LoadInstVec;
	std::vector<StoreInst*> StoreInstVec;
	LdStScopeAnalysis(R, LoadInstVec, StoreInstVec);
	
	std::vector<LoadInst*> ExternLoadInstVec;
	std::vector<StoreInst*> ExternStoreInstVec;
	LdStAliasAnalysis(LoadInstVec, StoreInstVec, AA, DL, DT, PDT, 
										ExternLoadInstVec, ExternStoreInstVec);
 
	unsigned int CurRelaxedRegionID = CPI->RRGNToIdMap[R];
	for(LoadInst* LI : ExternLoadInstVec){
		Value* Ptr = LI->getPointerOperand();
		Value* Val = LI;
		
		InstrumentRecord IR_PTR(RRGN_TYPE_DEF, LD_PTR_DEF, CurRelaxedRegionID,
			CPI->ValRefToIdMap[std::make_pair(Ptr, LI)], Ptr, true, LI);
		CPI->InstrumentRecordVec.push_back(IR_PTR);
		
		InstrumentRecord IR_VAL(RRGN_TYPE_DEF, LD_VAL_DEF, CurRelaxedRegionID,
			CPI->ValRefToIdMap[std::make_pair(LI, LI)], Val, false, LI);
		CPI->InstrumentRecordVec.push_back(IR_VAL);
	}
	
	for(StoreInst* SI : ExternStoreInstVec){
		Value* Ptr = SI->getPointerOperand();
		Value* Val = SI->getValueOperand();
		
		InstrumentRecord IR_PTR(RRGN_TYPE_DEF, ST_PTR_DEF, CurRelaxedRegionID,
			CPI->ValRefToIdMap[std::make_pair(Ptr, SI)], Ptr, true, SI);
		CPI->InstrumentRecordVec.push_back(IR_PTR);
		
		InstrumentRecord IR_VAL(RRGN_TYPE_DEF, ST_VAL_DEF, CurRelaxedRegionID,
			CPI->ValRefToIdMap[std::make_pair(Val, SI)], Val, true, SI);
		CPI->InstrumentRecordVec.push_back(IR_VAL);
	}
	
	std::set<Value*> VarInSet;
	std::set<Value*> VarOutSet;
	RelaxedRegionInputOutputValueAnalysis(R, VarInSet, VarOutSet);
	
	for(Value* VI: VarInSet){
		Instruction* RefLoc = R->getEntryInst();
		InstrumentRecord IR(RRGN_TYPE_DEF, VAL_IN_DEF, CurRelaxedRegionID,
			CPI->ValToIdMap[VI], VI, false, RefLoc);
		CPI->InstrumentRecordVec.push_back(IR);
	}
	
	for(Value* VO: VarOutSet){
		if(Instruction* VOI = dyn_cast<Instruction>(VO)){
			InstrumentRecord IR(RRGN_TYPE_DEF, REF_OUT_DEF, CurRelaxedRegionID,
				CPI->ValRefToIdMap[std::make_pair(VOI, VOI)], VOI, false, VOI);
			CPI->InstrumentRecordVec.push_back(IR);
		}
		else{
			assert( 0 && "No output value of region could be llvm::Value type!\n");
		}
	}
  
  return true;
}

// ***************************************//
// *** Instruction Value Analysis Pass ***//
// ***************************************//
bool CRVPAnalysis::InstructionValueAnalysisImpl(Instruction *ProfInst) {
  
	#ifdef DEBUG_MSG
	errs() << "\tRun InstructionValueAnalysis On ";
	ProfInst->print(errs());
	errs() << ";\n";
  #endif

	unsigned int CurInstID = CPI->ValToIdMap[ProfInst];
	if(LoadInst* LI = dyn_cast<LoadInst>(ProfInst)){
		Value* Ptr = LI->getPointerOperand();
		Value* Val = LI;
		
		#ifdef PROF_INST_SRC
		InstrumentRecord IR_PTR(INST_TYPE_DEF, LD_PTR_DEF, CurInstID,
			CPI->ValRefToIdMap[std::make_pair(Ptr, LI)], Ptr, true, LI);
		CPI->InstrumentRecordVec.push_back(IR_PTR);
		#endif

		InstrumentRecord IR_VAL(INST_TYPE_DEF, LD_VAL_DEF, CurInstID,
			CPI->ValRefToIdMap[std::make_pair(LI, LI)], Val, false, LI);
		CPI->InstrumentRecordVec.push_back(IR_VAL);
	}
	else if(StoreInst* SI = dyn_cast<StoreInst>(ProfInst)){
		Value* Ptr = SI->getPointerOperand();
		Value* Val = SI->getValueOperand();
		
		#ifdef PROF_INST_SRC
		InstrumentRecord IR_PTR(INST_TYPE_DEF, ST_PTR_DEF, CurInstID,
			CPI->ValRefToIdMap[std::make_pair(Ptr, SI)], Ptr, true, SI);
		CPI->InstrumentRecordVec.push_back(IR_PTR);

		InstrumentRecord IR_VAL(INST_TYPE_DEF, ST_VAL_DEF, CurInstID,
			CPI->ValRefToIdMap[std::make_pair(Val, SI)], Val, true, SI);
		CPI->InstrumentRecordVec.push_back(IR_VAL);
		#endif
	}
	else{
		#ifdef PROF_INST_SRC
		for(Value* OP: ProfInst->operands()){
			InstrumentRecord IR(INST_TYPE_DEF, REF_IN_DEF, CurInstID,
				CPI->ValRefToIdMap[std::make_pair(OP, ProfInst)], OP, true, ProfInst);
			CPI->InstrumentRecordVec.push_back(IR);
		}
		#endif
		InstrumentRecord IR(INST_TYPE_DEF, REF_OUT_DEF, CurInstID,
			CPI->ValRefToIdMap[std::make_pair(ProfInst, ProfInst)], ProfInst, false, ProfInst);
		CPI->InstrumentRecordVec.push_back(IR);
	}
  
  return true;
}

// ***********************************************//
// *** Code Region Value Profile Analysis Pass ***//
// ***********************************************//
bool CRVPAnalysis::runOnFunction(Function &F){
	#ifdef DEBUG_MSG
	errs() << "Run CRVPAnalysis Pass On " << F.getName() << ";\n";
	#endif
	Module& M = *F.getParent();
	
	DL = &M.getDataLayout();
	CPI = &getAnalysis<CRVPInit>().CPI;
	
	LI = &getAnalysis<LoopInfoWrapperPass>().getLoopInfo();
	AA = &getAnalysis<AAResultsWrapperPass>().getAAResults();
	DT = &getAnalysis<DominatorTreeWrapperPass>().getDomTree();
	PDT = &getAnalysis<PostDominatorTreeWrapperPass>().getPostDomTree();
	
	#ifdef USER_DEFINE_MODE
	CPI->ProfInfoLoad();
	#endif
	
	#ifdef USER_DEFINE_MODE
	if(CPI->ProfFuncVec.find(F) != CPI->ProfFuncVec.end())
		FunctionValueAnalysisImpl(F, AA, DT, PDT, DL);
	#else
	FunctionValueAnalysisImpl(F, AA, DT, PDT, DL);	
	#endif
	
	#ifdef USER_DEFINE_MODE
	for(BasicBlock* B : CPI->ProfLoopVec){
		if(&F != B->getParent() continue;
		Loop* L = LI->getLoopFor(B);
		LoopValueAnalysisImpl(L, AA, DT, PDT, DL);
	}
	#else
	for(LoopInfo::iterator LII = LI->begin(), LIE = LI->end(); LII != LIE; ++LII){
		Loop* L = *LII;
		LoopValueAnalysisImpl(L, AA, DT, PDT, DL);
	}
	#endif
	
  #ifdef USER_DEFINE_MODE
	for(Instruction* I : CPI->ProfInstVec){
		if(&F != I->getParent()->getParent()) continue;
		InstructionValueAnalysisImpl(I);
	}
	#endif

	return true;
}

void CRVPAnalysis::getAnalysisUsage(AnalysisUsage &AU) const{
	AU.setPreservesAll();

	AU.addRequired<CRVPInit>();
	AU.addRequired<LoopInfoWrapperPass>();
	AU.addRequired<AAResultsWrapperPass>();
	AU.addRequired<DominatorTreeWrapperPass>();
	AU.addRequired<PostDominatorTreeWrapperPass>();
}

char CRVPAnalysis::ID = 0;
static RegisterPass<CRVPAnalysis> CRVPAnlys("crvp-anlys", "Code Region Value Profile Analysis");


// *************************************************//
// *** Code Region Value Profile Instrument Pass ***//
// *************************************************//
void CRVPInstrument::createDeclarations(Module &M){
  LLVMContext& MContext = M.getContext();
  // In the case when different AnalysisType shares a value reference to
  // profile, only instrument one profile call;
  // In the case when different AnalysisType shares a value (without specific
  // reference), instrument different profile calls;
  CRVP_MODULE_INIT = M.getOrInsertFunction("CRVP_MODULE_INIT", llvm::Type::getVoidTy(MContext),
  (Type*)0);
  
	CRVP_MODULE_EXIT = M.getOrInsertFunction("CRVP_MODULE_EXIT", llvm::Type::getVoidTy(MContext),
  (Type*)0);
  
  CRVP_LOOP_BACK = M.getOrInsertFunction("CRVP_LOOP_BACK", llvm::Type::getVoidTy(MContext),
  llvm::Type::getInt32Ty(MContext), (Type*)0);
  
  CRVP_LOOP_EXIT = M.getOrInsertFunction("CRVP_LOOP_EXIT", llvm::Type::getVoidTy(MContext),
  llvm::Type::getInt32Ty(MContext), (Type*)0);
  
  CRVP_RRGN_ENTRY = M.getOrInsertFunction("CRVP_RRGN_ENTRY", llvm::Type::getVoidTy(MContext),
  llvm::Type::getInt32Ty(MContext), (Type*)0);
  
  CRVP_RRGN_EXIT = M.getOrInsertFunction("CRVP_RRGN_EXIT", llvm::Type::getVoidTy(MContext),
  llvm::Type::getInt32Ty(MContext), (Type*)0);
 
  // CRVP_FUN_VAL_INT_1( uint32_t VAL_ID, uint1_t VAL )
  CRVP_VAL_INT_1 = M.getOrInsertFunction("CRVP_VAL_INT_1", llvm::Type::getVoidTy(MContext),
    llvm::Type::getInt32Ty(MContext),
    llvm::Type::getInt1Ty(MContext), (Type *)0);
  
  // CRVP_FUN_VAL_INT_8( uint32_t VAL_ID, uint8_t VAL )
  CRVP_VAL_INT_8 = M.getOrInsertFunction("CRVP_VAL_INT_8", llvm::Type::getVoidTy(MContext),
    llvm::Type::getInt32Ty(MContext),
    llvm::Type::getInt8Ty(MContext), (Type *)0);
  
  // CRVP_FUN_VAL_INT_16( uint32_t VAL_ID, uint16_t VAL )
  CRVP_VAL_INT_16 = M.getOrInsertFunction("CRVP_VAL_INT_16", llvm::Type::getVoidTy(MContext),
    llvm::Type::getInt32Ty(MContext),
    llvm::Type::getInt16Ty(MContext), (Type *)0);
  
  // CRVP_FUN_VAL_INT_32( uint32_t VAL_ID, uint32_t VAL )
  CRVP_VAL_INT_32 = M.getOrInsertFunction("CRVP_VAL_INT_32", llvm::Type::getVoidTy(MContext),
    llvm::Type::getInt32Ty(MContext),
    llvm::Type::getInt32Ty(MContext), (Type *)0);
  
  // CRVP_FUN_VAL_INT_64( uint32_t VAL_ID, uint64_t VAL )
  CRVP_VAL_INT_64 = M.getOrInsertFunction("CRVP_VAL_INT_64", llvm::Type::getVoidTy(MContext),
    llvm::Type::getInt32Ty(MContext),
    llvm::Type::getInt64Ty(MContext), (Type *)0);
  
  // CRVP_FUN_VAL_FP_32( uint32_t VAL_ID, float VAL )
  CRVP_VAL_FP_32 = M.getOrInsertFunction("CRVP_VAL_FP_32", llvm::Type::getVoidTy(MContext),
    llvm::Type::getInt32Ty(MContext),
    llvm::Type::getFloatTy(MContext), (Type *)0);
  
  // CRVP_FUN_VAL_FP_64( uint32_t VAL_ID, double VAL )
  CRVP_VAL_FP_64 = M.getOrInsertFunction("CRVP_VAL_FP_64", llvm::Type::getVoidTy(MContext),
    llvm::Type::getInt32Ty(MContext),
    llvm::Type::getDoubleTy(MContext), (Type *)0);  
}

bool CRVPInstrument::InstrumentValProfCall(InstrumentRecord& IR, Module& M, unsigned int& ProfType){
  unsigned int AnlysType = IR.AnlysType;
  unsigned int AnlysID = IR.AnlysID;
  bool ValAsRef = !(IR.ValueType == VAL_IN_DEF || IR.ValueType == VAL_OUT_DEF);
  
  Value *V = IR.ProfVal;
  Type* VT = V->getType();
  
  LLVMContext& MC = M.getContext();
  
  bool ValAsPtr = VT->isPointerTy();
  bool ValAsInt = VT->isIntegerTy();
  bool ValAsFP = VT->isFloatingPointTy();
  
  //FIXME: Current support profile for pointer, interger and floating point
  //       type in llvm.
  if(!ValAsPtr && !ValAsInt && !ValAsFP)
    return false;
   
  std::vector<Value*> Args(2);
  if(ValAsPtr){
    PtrToIntInst* PTI = new PtrToIntInst(V, llvm::Type::getInt64Ty(MC));
    SafeInsert(PTI, IR.InsertBeforePos, IR.Pos);
    
    Args[0] = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), InstrumentID[INT64_TYPE_DEF]++);
    Args[1] = PTI;
    
    CallInst* CI;
    CI = CallInst::Create(CRVP_VAL_INT_64, Args, "");
    ProfType = INT64_TYPE_DEF;
    SafeInsert(CI, false, PTI);
  }
  else if(ValAsInt){
    Args[1] = V;
    
    CallInst* CI;
    if(VT == llvm::Type::getInt1Ty(MC)){
      Args[0] = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), InstrumentID[INT1_TYPE_DEF]++);
      CI = CallInst::Create(CRVP_VAL_INT_1, Args, "");
      ProfType = INT1_TYPE_DEF;
    }
    else if(VT == llvm::Type::getInt8Ty(MC)){
      Args[0] = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), InstrumentID[INT8_TYPE_DEF]++);
      CI = CallInst::Create(CRVP_VAL_INT_8, Args, "");
      ProfType = INT8_TYPE_DEF;
    }
    else if(VT == llvm::Type::getInt16Ty(MC)){
      Args[0] = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), InstrumentID[INT16_TYPE_DEF]++);
      CI = CallInst::Create(CRVP_VAL_INT_16, Args, "");
      ProfType = INT16_TYPE_DEF;
    }
    else if(VT == llvm::Type::getInt32Ty(MC)){
      Args[0] = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), InstrumentID[INT32_TYPE_DEF]++);
      CI = CallInst::Create(CRVP_VAL_INT_32, Args, "");
      ProfType = INT32_TYPE_DEF;
    }
    else if(VT == llvm::Type::getInt64Ty(MC)){
      Args[0] = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), InstrumentID[INT64_TYPE_DEF]++);
      CI = CallInst::Create(CRVP_VAL_INT_64, Args, "");
      ProfType = INT64_TYPE_DEF;
    }
    else
      assert( 0 && "Missing interger type!\n");
    SafeInsert(CI, IR.InsertBeforePos, IR.Pos);
  }
  else if(ValAsFP){
    if(VT == llvm::Type::getHalfTy(MC)){
      CastInst* CastI = CastInst::CreateFPCast(V, llvm::Type::getFloatTy(MC));
      SafeInsert(CastI, IR.InsertBeforePos, IR.Pos);
     
      Args[0] = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), InstrumentID[FP32_TYPE_DEF]++);
      Args[1] = CastI;
      
      CallInst* CI;
      CI = CallInst::Create(CRVP_VAL_FP_32, Args, "");
      ProfType = FP32_TYPE_DEF;
      SafeInsert(CI, false, CastI);
    }
    else if(VT == llvm::Type::getFloatTy(MC)){
      Args[0] = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), InstrumentID[FP32_TYPE_DEF]++);
      Args[1] = V;
      
      CallInst* CI;
      CI = CallInst::Create(CRVP_VAL_FP_32, Args, "");
      ProfType = FP32_TYPE_DEF;
      SafeInsert(CI, IR.InsertBeforePos, IR.Pos);
    }
    else if(VT == llvm::Type::getDoubleTy(MC)){
      Args[0] = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), InstrumentID[FP64_TYPE_DEF]++);
      Args[1] = V;
      
      CallInst* CI;
      CI = CallInst::Create(CRVP_VAL_FP_64, Args, "");
      ProfType = FP64_TYPE_DEF;
      SafeInsert(CI, IR.InsertBeforePos, IR.Pos);
    }
    /*else if(VT == llvm::Type::getX86FP80Ty(MC) || VT == llvm::Type::getFP128Ty(MC) || VT == llvm::Type::getPPC_FP128Ty(MC))*/
    else { 
      CastInst* CastI = CastInst::CreateFPCast(V, llvm::Type::getDoubleTy(MC));
      SafeInsert(CastI, IR.InsertBeforePos, IR.Pos);
      
      Args[0] = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), InstrumentID[FP64_TYPE_DEF]++);
      Args[1] = CastI;
      
      CallInst* CI;
      CI = CallInst::Create(CRVP_VAL_FP_64, Args, "");
      ProfType = FP64_TYPE_DEF;
      SafeInsert(CI, false, CastI);
    }/*
    else
      assert( 0 && "Mssing floating point type!\n");
    */
  }

  return true;
}

bool CRVPInstrument::runOnModule(Module &M){
  // Debug Msg
  errs() << "Run CRVPInstrument On " << M.getName() << ";\n";
	
	CPI = &getAnalysis<CRVPInit>().CPI;
  
  createDeclarations(M);

  // Instrument CRVP_MODULE_INIT
  for(Module::iterator MI = M.begin(), ME = M.end(); MI != ME; ++MI){
    Function* F = &*MI;
    //TODO: Only Valid For C Program
    if(F->getName() == "main"){
      std::vector<Value*> Args;
      CallInst* INIT_CI = CallInst::Create(CRVP_MODULE_INIT, Args, "");
      Instruction* Pos = &*(F->getEntryBlock().getFirstInsertionPt());
      SafeInsert(INIT_CI, true, Pos);
      
			CallInst* EXIT_CI = CallInst::Create(CRVP_MODULE_EXIT, Args, "");
			for(Function::iterator FI = F->begin(), FE = F->end(); FI != FE; ++FI){
				BasicBlock* B = &*FI;
				for(BasicBlock::iterator BI = B->begin(), BE = B->end(); BI != BE; ++BI){
					Instruction* I = &*BI;
					if(isa<ReturnInst>(I))
						SafeInsert(EXIT_CI, true, I);
				}
			}
			break;
    }
  }
  
  // Instrument CRVP_LOOP_BACK & CRVP_LOOP_EXIT
  // TODO: Better methodology is split edges and insert basicblocks in the
  // middle of edges.
  for(BasicBlock* H : CPI->ProfLoopVec){
    unsigned int LoopID = CPI->BBToIdMap[H];
    std::vector<Value*> Args(1);
    Args[0] = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), LoopID);
    for(BasicBlock* LatchBlock : CPI->ProfLoopLatches[H]){
      Instruction* BackEdge = LatchBlock->getTerminator();
      CallInst* CI = CallInst::Create(CRVP_LOOP_BACK, Args, "");
      SafeInsert(CI, true, BackEdge);
    }
    for(BasicBlock* ExitBlock : CPI->ProfLoopExitBlocks[H]){
      CallInst* EXIT_CI = CallInst::Create(CRVP_LOOP_EXIT, Args, "");
      SafeInsert(EXIT_CI, true, &*ExitBlock->getFirstInsertionPt());
    }
  }
  
  // Instrument CRVP_RRGN_ENTRY & CRVP_RRGN_EXIT
  // TODO: Better methodology is split edges and insert basicblocks in the
  // middle of edges.
  for(RelaxedRegion* R : CPI->ProfRRgnVec){
    unsigned int RRgnID = CPI->RRGNToIdMap[R];
    std::vector<Value*> Args(1);
    Args[0] = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), RRgnID);
    CallInst* CI = CallInst::Create(CRVP_RRGN_ENTRY, Args, "");
    SafeInsert(CI, true, R->getEntryInst());
    for(Instruction* I : R->getExitInsts()){
      CallInst* CI = CallInst::Create(CRVP_RRGN_EXIT, Args, "");
      SafeInsert(CI, true, I);
    }
  }
  
	std::vector<ANLYS_INFO> AnlysInfoVec;

  unsigned int AnlysTypeNum;
  unsigned int ValueTypeNum;
 
  std::map<Value*, unsigned int> ProfTypeMap;
  std::map<Value*, unsigned int> ProfIDMap;

  for(InstrumentRecord IR: CPI->InstrumentRecordVec){
    ANLYS_INFO AI;
		Value* V = IR.ProfVal;
    
    unsigned int ProfType = NONE_TYPE_DEF;
    unsigned int ProfID = -1;
    
		bool ValAsRef = !(IR.ValueType == VAL_IN_DEF || IR.ValueType == VAL_OUT_DEF); 
		if(ValAsRef){
			if(ProfTypeMap.find(V) == ProfTypeMap.end()){
				InstrumentValProfCall(IR, M, ProfType);
				ProfID = InstrumentID[ProfType] - 1;
				ProfTypeMap[V] = ProfType;
				ProfIDMap[V] = ProfID;
			}
			else{
				ProfType = ProfTypeMap[V];
				ProfID = ProfIDMap[V];
			}
		}
		else{
			InstrumentValProfCall(IR, M, ProfType);
			ProfID = InstrumentID[ProfType] - 1;
		}
    
    if(ProfType == NONE_TYPE_DEF){
    	ProfID = -1;
		}
   
    AI.ANLYS_TYPE = IR.AnlysType;
   	AI.ANLYS_ID = IR.AnlysID;
   	
		AI.VAL_TYPE = IR.ValueType;
		AI.VAL_ID = IR.ValueID;
		
  	AI.PROF_TYPE = ProfType;
		AI.PROF_ID = ProfID;
		
		AnlysInfoVec.push_back(AI);
	}
	
	ANLYS_SUM AS;
	AS.ANLYS_SIZE = AnlysInfoVec.size();
	for( int i = 0; i < 7; ++ i )
		AS.PROF_SIZE[i] = InstrumentID[i];
	
	FILE *fp;
	fp = fopen("anlysdata.crvp", "wb");
	fwrite(&AS, sizeof(ANLYS_SUM), 1, fp);
	fwrite(&AnlysInfoVec[0], sizeof(ANLYS_INFO), AnlysInfoVec.size(), fp);
	fclose(fp);
  
	return true;
}

void CRVPInstrument::getAnalysisUsage(AnalysisUsage &AU) const{
  AU.addRequired<CRVPInit>();
	AU.addRequired<CRVPAnalysis>(); 
	AU.addRequired<LoopInfoWrapperPass>();
}

char CRVPInstrument::ID = 0;
static RegisterPass<CRVPInstrument> CRVPInstr("crvp-instrument", "Code Region Value Profile Instrument");
