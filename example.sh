#!/bin/bash

fname=example
libcrvp=/home/example/crvp/libcrvp.so
crvp_rt_dir=/home/example/crvp/rt

mkdir build
clang -fno-vectorize -fno-unroll-loops -emit-llvm -w -O3 -o build/$fname.bc -c $fname.c
opt -loop-simplify < build/$fname.bc > build/$fname.ls.bc
opt -load $libcrvp -crvp-init -crvp-anlys -crvp-instrument build/$fname.ls.bc -o build/$fname.crvp.bc

llc build/$fname.crvp.bc -o build/$fname.crvp.s
gcc -w -g -O3 -o build/rt.o -c $crvp_rt_dir/CRVP_RT.c
gcc -w -g -O3 -o build/stats.o -c $crvp_rt_dir/stats.c
gcc -w -g -O3 -o build/$fname.crvp build/$fname.crvp.s build/rt.o build/stats.o
./build/$fname.crvp $1

opt -load $libcrvp -crvp-init -crvp-load -crvp-func-test -crvp-loop-test build/$fname.ls.bc -o build/$fname.crvp.test.bc
