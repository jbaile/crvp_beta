/* 
 * CRVP_Load.h
 * Author: Shikai Li
 *
 */

#ifndef CRVP_LOAD_H
#define CRVP_LOAD_H

#include "CRVP_Analysis.h"
#include "CRVP_Types.h"

#include <stdio.h>

using namespace llvm;

typedef std::pair<Value*, Value*> ValueRef;

struct CRVP_LOAD_INFO{
	bool valid = false;

	ANLYS_SUM AS;
	std::vector<ANLYS_INFO> AI_VEC;

	std::vector<STATS_BOOL> SB_VEC;
	std::vector<std::vector<STATS_INT> > SI_VEC;
	std::vector<std::vector<STATS_FP> >  SF_VEC;
};

struct CRVP_PROF_INFO{
	unsigned int AnlysType;
	unsigned int AnlysID;

	std::vector<Value*> VAL_IN;
	std::vector<Value*> VAL_OUT;	
	std::vector<ValueRef> REF_IN;
	std::vector<ValueRef> REF_OUT;
	std::vector<ValueRef> LD_PTR;
	std::vector<ValueRef> LD_VAL;
	std::vector<ValueRef> ST_PTR;
	std::vector<ValueRef> ST_VAL;		
	
	std::map<ValueRef, STATS_UNI> PROF_DATA_MAP;
};

void LoadCRVPProfData(std::string AnlysFileName, std::string ProfFileName);

// GetAnlysID() -> GetProfInfo() -> Use PROF_DATA_MAP to access profile data
namespace llvm {
  struct CRVPLoad : public ModulePass {
    public:
      static char ID;
			
			CRVP_INFO* CPI;
			CRVP_LOAD_INFO CLI;
      CRVPLoad() : ModulePass(ID){};
			
      virtual bool runOnModule(Module &M);
      virtual void getAnalysisUsage(AnalysisUsage &AU) const;
			
			std::set<Function*> 		GetProfFuncSet();
			std::set<BasicBlock*> 	GetProfLoopBlockSet();
			std::map<ValueRef, STATS_UNI> GetProfValueMap();
			
			unsigned int GetAnlysID(Function* F);
			unsigned int GetAnlysID(BasicBlock* B);
			unsigned int GetAnlysID(Instruction* I);
			
			bool DumpProfInfo(unsigned int AnlysType, unsigned int AnlysID);
			CRVP_PROF_INFO GetProfInfo(unsigned int AnalysisType, unsigned int AnalysisID);
			
		private:
			bool LoadCRVPProfData(std::string AnlysFileName, std::string ProfFileName);	
  		bool GetProfData(unsigned int ProfType, unsigned int ProfID, STATS_UNI& SU);
	};
 
 	// To test CRVPLoad Result
	struct CRVPFunctionTest : public FunctionPass {
    public:
      static char ID;
      CRVPFunctionTest() : FunctionPass(ID){}
      
      bool runOnFunction(Function &F);
      void getAnalysisUsage(AnalysisUsage &AU) const;
  };
  
  struct CRVPLoopTest : public LoopPass {
    public:
      static char ID;
      CRVPLoopTest() : LoopPass(ID){}
      
      bool runOnLoop(Loop *L, LPPassManager &LPM);
      void getAnalysisUsage(AnalysisUsage &AU) const;
  };
}

#endif // CRVP_LOAD_H
